package nanboonma.nantawat.lab8;

import javax.swing.SwingUtilities;

/**
 * This program MyFrameV3 extends from class MyFrameV2 will add paintCompenent
 * and show interface.
 * 
 * 
 * Author: Nantawat Nanboonma ID: 613040500-3 Sec: 2 Date: March 28, 2019
 * 
 * @author �ѹ��Ѳ��
 *
 */
public class MyFrameV3 extends MyFrameV2 {
	/**
	 * this is a constructor that super text from MyFrameV2 and tranform class to be
	 * like MyFrameV2.
	 * 
	 * @param title
	 */
	public MyFrameV3(String text) {
		super(text);

	}
	
	/**
	 * this main will display a result from code.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});

	}
	
	/**
	 * this method is the method that run JFrame process code.
	 */
	public static void createAndShowGUI() {
		MyFrameV3 msw = new MyFrameV3("My Frame V3");
		msw.addComponents();
		msw.setFrameFeatures();
	}
	
	/**
	 * this method will add paintComponent from MyCanvasV3 in JFrame
	 */
	protected void addComponents() {
		add(new MyCanvasV3());
	}

}
