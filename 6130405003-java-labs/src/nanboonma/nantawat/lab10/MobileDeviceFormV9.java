package nanboonma.nantawat.lab10;

import java.awt.Color;
import java.awt.event.ActionEvent;

import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JColorChooser;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileSystemView;

public class MobileDeviceFormV9 extends MobileDeviceFormV8 implements ActionListener {
	/**
	 * declare variable of class JFileChooser.
	 */
	protected JFileChooser fc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
	/**
	 * declare variable of class Color
	 */
	protected Color colorChoose;
	

	/**
	 * this is constructors of MobileDeviceFormV9 that super code from
	 * MobileDeviceFormV8.
	 */
	public MobileDeviceFormV9(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}

	/**
	 * this method will get Source when user clicked and progress condition when
	 * user clicked.
	 */
	public void actionPerformed(ActionEvent event) {
		super.actionPerformed(event);
		Object src = event.getSource();

		if (src == openItem) {
			int returnVal = fc.showOpenDialog(null);
			if (returnVal == fc.APPROVE_OPTION) {
				java.io.File selectedFile = fc.getSelectedFile();
				JOptionPane.showMessageDialog(null, "Opening file " + selectedFile.getName());
			} else if (returnVal == fc.CANCEL_OPTION) {
				JOptionPane.showMessageDialog(null, "Open command cancelled by user");
			}

		} else if (src == saveItem) {
			int returnVal = fc.showSaveDialog(null);
			if (returnVal == fc.APPROVE_OPTION) {
				java.io.File selectedFile = fc.getSelectedFile();
				JOptionPane.showMessageDialog(null, "Saving file " + selectedFile.getName());
			} else if (returnVal == fc.CANCEL_OPTION) {
				JOptionPane.showMessageDialog(null, "Save command cancelled by user");
			}
		} else if (src == redItem) {
			txtArea.setBackground(Color.RED);
			txtArea.setForeground(Color.BLACK);
		} else if (src == greenItem) {
			txtArea.setBackground(Color.GREEN);
			txtArea.setForeground(Color.BLACK);
		} else if (src == blueItem) {
			txtArea.setBackground(Color.BLUE);
			txtArea.setForeground(Color.BLACK);
		} else if (src == custom) {
			colorChoose = JColorChooser.showDialog(null, "Choose a Color", txtArea.getBackground());
			if (colorChoose != null)
				txtArea.setBackground(colorChoose);
			txtArea.setForeground(Color.BLACK);
			;
		} else if (src == exitItem) {
			System.exit(0);
		}

	}

	/**
	 * this method will adListener to object.
	 */
	public void addListeners() {
		super.addListeners();
		openItem.addActionListener(this);
		saveItem.addActionListener(this);
		exitItem.addActionListener(this);
		redItem.addActionListener(this);
		greenItem.addActionListener(this);
		blueItem.addActionListener(this);
		custom.addActionListener(this);

	}

	/**
	 * this method is the method that run JFrame process code.
	 */
	public static void createAndShowGUI() {
		MobileDeviceFormV9 mobileDeviceFormV9 = new MobileDeviceFormV9("Mobile Device Form V9");
		mobileDeviceFormV9.addComponents();
		mobileDeviceFormV9.initComponents();
		mobileDeviceFormV9.addMenus();
		mobileDeviceFormV9.enableKeyboard();
		mobileDeviceFormV9.addListeners();
		mobileDeviceFormV9.setFrameFeatures();

	}

	/**
	 * this main will display a result from code.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}
