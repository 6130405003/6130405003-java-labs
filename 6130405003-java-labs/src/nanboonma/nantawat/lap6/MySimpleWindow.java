package nanboonma.nantawat.lap6;

import javax.swing.*;
import java.awt.*;

/**
 * 
 * This program MySimpleWindow extends from class JFrame will display JFrame
 * with cancel and ok button.
 * 
 * Author: Nantawat Nanboonma ID: 613040500-3 Sec: 2 Date: February 27, 2019
 * 
 */
public class MySimpleWindow extends JFrame {
	/**
	 * this panel was made for get cancel and ok button.
	 */
	protected JPanel buttonPanel;
	/**
	 * this is the main panel of program and take any panel into it.
	 */
	protected JPanel panel;
	/**
	 * this is a cancel button
	 */
	protected JButton cancelButton;
	/**
	 * this is a ok button
	 */
	protected JButton okButton;

	/**
	 * this is a constructor that super title from JFrame and tranfrom class to be
	 * like JFrame.
	 * 
	 * @param title
	 */
	public MySimpleWindow(String title) {
		super(title);
	}

	/**
	 * this method is the operation of cancel and ok button to set it in main panel.
	 */
	protected void addComponents() {

		cancelButton = new JButton("Cancel");
		okButton = new JButton("OK");
		panel = new JPanel();
		// panel.setLayout(new GridLayout(0, 1, 5, 5));
		buttonPanel = new JPanel();
		buttonPanel.add(cancelButton);
		buttonPanel.add(okButton);
		panel = (JPanel) this.getContentPane();
		panel.setLayout(new BorderLayout());
		panel.add(buttonPanel, BorderLayout.SOUTH);

	}

	/**
	 * this method made for set location of JFrame.
	 */
	protected void setFrameFeatures() {
		this.pack();
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setVisible(true);

	}

	/**
	 * this method is the method that run JFrame process code.
	 */
	public static void createAndShowGUI() {
		MySimpleWindow mySimpleWindow = new MySimpleWindow("My Simple Window");
		mySimpleWindow.addComponents();
		mySimpleWindow.setFrameFeatures();

	}

	/**
	 * this main will display a result from code.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}
