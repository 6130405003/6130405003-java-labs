package nanboonma.nantawat.lab8;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

/**
 * This program MyFrameV2 extends from class MyFrame will add paintCompenent and
 * show interface.
 * 
 * 
 * Author: Nantawat Nanboonma ID: 613040500-3 Sec: 2 Date: March 28, 2019
 * 
 * @author �ѹ��Ѳ��
 *
 */
public class MyFrameV2 extends MyFrame {
	/**
	 * this is a constructor that super text from MyFrame and tranform class to be
	 * like MyFrame.
	 * 
	 * @param title
	 */
	public MyFrameV2(String text) {
		super(text);

	}

	/**
	 * this main will display a result from code.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});

	}

	/**
	 * this method is the method that run JFrame process code.
	 */
	public static void createAndShowGUI() {
		MyFrameV2 msw = new MyFrameV2("My Frame V2");
		msw.addComponents();
		msw.setFrameFeatures();
	}

	/**
	 * this method will add paintComponent from MyCanvasV2 in JFrame
	 */
	protected void addComponents() {
		add(new MyCanvasV2());
	}

}
