package nanboonma.nantawat.lab9;

import javax.swing.SwingUtilities;
/**
 * This program MyFrameV6 extends from class MyFrameV5 will add paintCompenent
 * and show interface.
 * 
 * 
 * Author: Nantawat Nanboonma ID: 613040500-3 Sec: 2 Date: April 7, 2019
 * 
 * @author �ѹ��Ѳ��
 *
 */
public class MyFrameV6 extends MyFrameV5 {
	/**
	 * this is a constructor that super text from MyFrameV2 and tranfrom class to be
	 * like MyFrameV5.
	 */
	public MyFrameV6(String text) {
		super(text);
		// TODO Auto-generated constructor stub
	}

	/**
	 * this main will display a result from code.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});

	}

	/**
	 * this method is the method that run JFrame process code.
	 */
	public static void createAndShowGUI() {
		MyFrameV6 msw = new MyFrameV6("My Frame V6");
		msw.addComponents();
		msw.setFrameFeatures();
	}

	/**
	 * this method will add paintComponent from MyCanvasV6 in JFrame
	 */
	protected void addComponents() {
		add(new MyCanvasV6());
	}

}
