/*
 * Interface Refuelable has one method called refuel().
 * Author: Nantawat Nanboonma
 * ID: 613040500-3
 * Sec: 2
 * Date: February 8, 2019
 * 
 */
package nanboonma.nantawat.lap4;
/**
 * Interface Refuelable has one method called refuel().
 * @author �ѹ��Ѳ��
 *
 */
public interface Refuelable {
	public void refuel();
}
