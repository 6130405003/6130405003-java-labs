package nanboonma.nantawat.lab10;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JComboBox;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import nanboonma.nantawat.lab7.MobileDeviceFormV6;

/**
 * This program MobileDeviceFormV7 extends from class MobileDeviceFormV6 from
 * lab7 this program will showMessageDialog when user change type, Os, platform and features
 *
 * 
 * Author: Nantawat Nanboonma ID: 613040500-3 Sec: 2 Date: April 16, 2019
 * 
 * @author �ѹ��Ѳ��
 *
 */

public class MobileDeviceFormV7 extends MobileDeviceFormV6
		implements ActionListener, ItemListener, ListSelectionListener {
	ListSelectionModel listSelectionModel;

	/**
	 * this is constructors of MobileDeviceFormV7 that super code from
	 * MobileDeviceFormV6.
	 */
	public MobileDeviceFormV7(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}

	/**
	 * this method will adListener to object.
	 */
	public void addListeners() {
		okButton.addActionListener(this);
		cancelButton.addActionListener(this);
		device.addActionListener(this);
		androidOs.addItemListener(this);
		ios.addItemListener(this);
		features.addListSelectionListener(this);
	}

	/**
	 * this method will set fill list selection mode to multiple interval and set
	 * androidsOs is selected.
	 */
	protected void addComponents() {
		super.addComponents();
		androidOs.setSelected(true);
		features.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		int[] numFeatures = { 0, 1, 3 };
		features.setSelectedIndices(numFeatures);
	}

	/**
	 * this method will get Source when user clicked and progress condition when
	 * user clicked.
	 */
	public void actionPerformed(ActionEvent event) {

		// TODO Auto-generated method stub
		Object src = event.getSource();

		if (src == okButton) {
			handleOKButton();

		} else if (src == cancelButton) {
			handleCancelButton();
		}

		if (src == device) {
			String nameType = (String) device.getSelectedItem();
			if (nameType.equals("Phone")) {
				JOptionPane.showMessageDialog(null, "Type is updated to Phone");
			} else if (nameType.equals("Tablet")) {
				JOptionPane.showMessageDialog(null, "Type is updated to Tablet");
			} else if (nameType.equals("Smart TV")) {
				JOptionPane.showMessageDialog(null, "Type is updated to Smart TV");
			}
		}
	}

	/**
	 * this method is condition to showMessageDialog when user clicked OKButton.
	 */
	public void handleOKButton() {
		List featSelect = features.getSelectedValuesList();
		String strFeatSelect = featSelect.toString().replace("[", "").replace("]", "");
		if (androidOs.getSelectedObjects() != null) {
			JOptionPane.showMessageDialog(null,
					"Brand Name: " + firstTxtField.getText() + ". Model Name: " + secondTxtField.getText()
							+ ". Weight: " + thirdTxtField.getText() + ". Price: " + fourthTxtField.getText() + "\nOS: "
							+ androidOs.getText() + "\nType: " + device.getSelectedItem() + "\nFeatures: "
							+ strFeatSelect + "\nReview: " + txtArea.getText());
		}
		if (ios.getSelectedObjects() != null) {
			JOptionPane.showMessageDialog(null,
					"Brand Name: " + firstTxtField.getText() + ". Model Name: " + secondTxtField.getText()
							+ ". Weight: " + thirdTxtField.getText() + ". Price: " + fourthTxtField.getText() + "\nOS:"
							+ ios.getText() + "\nType:" + device.getSelectedItem() + "\nFeatures:" + strFeatSelect
							+ "\nReview:" + txtArea.getText());
		}
	}

	/**
	 * this method is condition to set textArea to blank when user clicked
	 * CancelButton.
	 */
	public void handleCancelButton() {
		firstTxtField.setText(" ");
		secondTxtField.setText(" ");
		thirdTxtField.setText(" ");
		fourthTxtField.setText(" ");
		txtArea.setText(" ");
	}

	/**
	 * this method is the method that run JFrame process code.
	 */
	public static void createAndShowGUI() {
		MobileDeviceFormV7 mobileDeviceFormV7 = new MobileDeviceFormV7("Mobile Device Form V7");
		mobileDeviceFormV7.addComponents();
		mobileDeviceFormV7.initComponents();
		mobileDeviceFormV7.addMenus();
		mobileDeviceFormV7.addListeners();
		mobileDeviceFormV7.setFrameFeatures();

	}

	/**
	 * this main will display a result from code.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	/**
	 * this method will showMessageDialog when user click radioButton.
	 */
	@Override
	public void itemStateChanged(ItemEvent e) {
		JRadioButton source = (JRadioButton) e.getItemSelectable();
		String nameOs = source.getText();
		if (e.getStateChange() == ItemEvent.SELECTED) {
			JOptionPane.showMessageDialog(null, "Your os platform is now changed to " + nameOs);
		}

	}

	/**
	 * this method will showMessageDialog when user click listSelection.
	 */
	@Override
	public void valueChanged(ListSelectionEvent e) {
		JList source = (JList) e.getSource();
		List featSelect = source.getSelectedValuesList();
		String featListToStr = featSelect.toString().replace("[", "").replace("]", "");

		boolean isAdjusting = e.getValueIsAdjusting();
		if (!isAdjusting) {
			if (source.isSelectionEmpty()) {

			} else {
				JOptionPane.showMessageDialog(null, featListToStr);
			}
		}
	}
}
