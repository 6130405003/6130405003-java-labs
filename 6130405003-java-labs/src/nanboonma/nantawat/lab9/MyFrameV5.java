package nanboonma.nantawat.lab9;

import javax.swing.SwingUtilities;

import nanboonma.nantawat.lab8.MyCanvasV4;
import nanboonma.nantawat.lab8.MyFrameV4;
/**
 * This program MyFrameV5 extends from class MyFrameV4 will add paintCompenent
 * and show interface.
 * 
 * 
 * Author: Nantawat Nanboonma ID: 613040500-3 Sec: 2 Date: April 7, 2019
 * 
 * @author �ѹ��Ѳ��
 *
 */
public class MyFrameV5 extends MyFrameV4 {
	/**
	 * this is a constructor that super text from MyFrameV4 and tranfrom class to be
	 * like MyFrameV4.
	 * @param args
	 */
	public MyFrameV5(String text) {
		super(text);
	}

	/**
	 * this main will display a result from code.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});

	}

	/**
	 * this method is the method that run JFrame process code.
	 */
	public static void createAndShowGUI() {
		MyFrameV5 msw = new MyFrameV5("My Frame V5");
		msw.addComponents();
		msw.setFrameFeatures();
	}

	/**
	 * this method will add paintComponent from MyCanvasV5 in JFrame
	 */
	protected void addComponents() {
		add(new MyCanvasV5());
	}

}
