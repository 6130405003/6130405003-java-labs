package nanboonma.nantawat.lab8;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.geom.Rectangle2D;

/**
 * this class extends from Rectangle2D.Double and this will be class for draw or
 * fill Rectangle.
 * 
 * Author: Nantawat Nanboonma ID: 613040500-3 Sec: 2 Date: March 28, 2019
 * 
 * @author �ѹ��Ѳ��
 *
 */
public class MyBrick extends Rectangle2D.Double {
	/**
	 * this is the variable of width for fill rectangle.
	 */
	public final static int brickWidth = 80;
	/**
	 * this is the variable of height for fill rectangle.
	 */
	public final static int brickHeight = 20;
	
	/**
	 * this constructor made for accept two input parameter of x,y and get width and height of
	 * rectangle is brickWidth and brickHeight.
	 * 
	 * @param x
	 * @param y
	 */
	public MyBrick(int x, int y) {
		super(x, y, brickWidth, brickHeight);

	}

}
