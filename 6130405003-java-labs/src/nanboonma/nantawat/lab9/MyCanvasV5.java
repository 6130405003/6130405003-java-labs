package nanboonma.nantawat.lab9;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

import nanboonma.nantawat.lab8.MyBall;
import nanboonma.nantawat.lab8.MyCanvas;
import nanboonma.nantawat.lab8.MyCanvasV4;
/**
 * This program MyCanvasV5 extends from class MyCanvasV4 will draw ball
 * and ball can run in panel
 *
 * 
 * Author: Nantawat Nanboonma ID: 613040500-3 Sec: 2 Date: April 7, 2019
 * 
 * @author �ѹ��Ѳ��
 *
 */

public class MyCanvasV5 extends MyCanvasV4 implements Runnable {
	/**
	 * this is constructors of circle to draw or fill ball.
	 */
	protected MyBallV2 ball = new MyBallV2(0, super.HEIGHT / 2 - MyBall.diameter / 2);
	/**
	 * this is constructor of thread to order ball to run.
	 */
	protected Thread running = new Thread(this);
	/**
	 * this is constructors of MyCavasV5 to set Velocity of ball and run ball.
	 */
	public MyCanvasV5() {
		ball.ballVelX = 2;
		ball.ballVelY = 0;
		running.start();

	}
	/**
	 * this method will draw shape Graphics2D.
	 */
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setColor(Color.BLACK);
		g2d.fillRect(0, 0, WIDTH, HEIGHT);
		g2d.setColor(Color.WHITE);
		g2d.fill(ball);
	}
	/**
	 * this method is made for write code for ball to run.
	 */
	public void run() {
		while (true) {
			if (ball.x + MyBall.diameter >= super.WIDTH) {
				ball.ballVelX = 0;
				break;
			}
			ball.move();
			repaint();

			try {
				Thread.sleep(10);
			} catch (InterruptedException ex) {
			}
			// TODO Auto-generated method stub

		}

	}
}
