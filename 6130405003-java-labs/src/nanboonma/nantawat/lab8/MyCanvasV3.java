package nanboonma.nantawat.lab8;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.util.ArrayList;

/**
 * This program MyCanvasV3 extends from class MyCanvasV2 will draw and fill 4
 * ball and 10 brick.
 *
 * 
 * Author: Nantawat Nanboonma ID: 613040500-3 Sec: 2 Date: March 28, 2019
 * 
 * @author �ѹ��Ѳ��
 *
 */
public class MyCanvasV3 extends MyCanvasV2 {
	/**
	 * this is constructors of circle to draw or fill ball.
	 */
	protected MyBall ballV3;
	/**
	 * this is constructors of rectangle to draw or fill brick.
	 */
	protected MyBrick brickV3;
	/**
	 * this is shape array for add ball shape
	 */
	protected ArrayList<Shape> listBall = new ArrayList<Shape>();
	/**
	 * this is shape array for add brick shape
	 */
	protected ArrayList<Shape> listBrick = new ArrayList<Shape>();

	/**
	 * this method will draw shape Graphics2D
	 */
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, WIDTH + 100, HEIGHT + 100);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setColor(Color.WHITE);
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 2; j++) {
				ballV3 = new MyBall(0 + ((WIDTH - ballV3.diameter) * i), 0 + (HEIGHT - ballV3.diameter) * j);
				g2d.fill(ballV3);
				listBall.add(ballV3);
			}

		}
		brickV3 = new MyBrick(0, 0);
		for (int i = 0; i < 10; i++) {
			brickV3.x = 0 + i * brickV3.brickWidth;
			brickV3.y = HEIGHT / 2;
			g2d.setColor(Color.WHITE);
			g2d.fill(brickV3);
			listBrick.add(brickV3);
			g2d.setColor(Color.BLACK);
			g2d.setStroke(new BasicStroke(4));
			g2d.draw(brickV3);

		}
	}
}
