package nanboonma.nantawat.lab7;

import java.awt.Color;
import java.awt.Font;

import javax.swing.SwingUtilities;
/**
 * This program MobileDeviceFormV5 extends from class MobileDeviceFormV4 will
 * display like MobileDeviceFormV4 and just change font of text to "Serif" and set size and style of text.
 * 
 * Author: Nantawat Nanboonma ID: 613040500-3 Sec: 2 Date: March 5, 2019
 * 
 * @author �ѹ��Ѳ��
 *
 */
public class MobileDeviceFormV5 extends MobileDeviceFormV4 {
	
	public MobileDeviceFormV5(String title) {
		super(title);
		
	}
	/**
	 * this is a method that will change font of text in program.
	 */
	protected void initComponents() {
		
		Font labelFont = new Font("Serif", Font.PLAIN, 14);
		Font textFont = new Font("Serif", Font.BOLD, 14);
		firstnameLabel.setFont(labelFont);
		secondnameLabel.setFont(labelFont);
		thirdnameLabel.setFont(labelFont);
		fourthnameLabel.setFont(labelFont);
		features.setFont(labelFont);
		mobileOsLabel.setFont(labelFont);
		review.setFont(labelFont);
		Type.setFont(labelFont);
		firstTxtField.setFont(textFont);
		secondTxtField.setFont(textFont);
		thirdTxtField.setFont(textFont);
		fourthTxtField.setFont(textFont);
		txtArea.setFont(textFont);
		cancelButton.setForeground(Color.RED);
		okButton.setForeground(Color.BLUE);
		
		
	}
	/**
	 * this method is the method that run JFrame process code.
	 */
	public static void createAndShowGUI() {
		MobileDeviceFormV5 mobileDeviceFormV5 = new MobileDeviceFormV5("Mobile Device Form V5");
		mobileDeviceFormV5.addComponents();
		mobileDeviceFormV5.initComponents();
		mobileDeviceFormV5.addMenus();
		mobileDeviceFormV5.setFrameFeatures();

	}

	/**
	 * this main will display a result from code.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}
