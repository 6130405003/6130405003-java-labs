package nanboonma.nantawat.lab9;

import javax.swing.SwingUtilities;
/**
 * This program MyFrameV7 extends from class MyFrameV6 will add paintCompenent
 * and show interface.
 * 
 * 
 * Author: Nantawat Nanboonma ID: 613040500-3 Sec: 2 Date: April 7, 2019
 * 
 * @author �ѹ��Ѳ��
 *
 */
public class MyFrameV7 extends MyFrameV6 {
	/**
	 * this is a constructor that super text from MyFrameV7 and tranfrom class to be
	 * like MyFrameV6.
	 */
	public MyFrameV7(String text) {
		super(text);
		// TODO Auto-generated constructor stub
	}

	/**
	 * this main will display a result from code.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});

	}

	/**
	 * this method is the method that run JFrame process code.
	 */
	public static void createAndShowGUI() {
		MyFrameV7 msw = new MyFrameV7("My Frame V7");
		msw.addComponents();
		msw.setFrameFeatures();
	}

	/**
	 * this method will add paintComponent from MyCanvasV7 in JFrame
	 */
	protected void addComponents() {
		add(new MyCanvasV7());
	}

}
