package nanboonma.nantawat.lab8;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import javax.swing.JPanel;

/**
 * This program MyCanvas extends from class JPanel will draw and fill graphic
 * that like face of human
 * 
 * Author: Nantawat Nanboonma ID: 613040500-3 Sec: 2 Date: March 28, 2019
 * 
 * @author �ѹ��Ѳ��
 *
 */
public class MyCanvas extends JPanel {
	/**
	 * this is variable of WIDTH for set size of panel
	 */
	public final static int WIDTH = 800;
	/**
	 * this is variable of HEIGHT for set size of panel
	 */
	public final static int HEIGHT = 600;

	/**
	 * this constructor will super from JPanel and set size and color of panel
	 */
	public MyCanvas() {
		super();
		setPreferredSize(new Dimension(WIDTH, HEIGHT));
		setBackground(Color.BLACK);
	}

	/**
	 * this method will draw shape Graphics2D
	 */
	public void paintComponent(Graphics g) {
		int w = getWidth();
		int h = getHeight();
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.draw(new Ellipse2D.Double((w - 300) / 2, (h - 300) / 2, 300, 300));
		g2d.setColor(Color.WHITE);
		g2d.fill(new Ellipse2D.Double((w - 150) / 2, (h - 150) / 2, 30, 60));
		g2d.fill(new Ellipse2D.Double((w + 90) / 2, (h - 150) / 2, 30, 60));
		g2d.fillRect((w - 100) / 2, (h + 150) / 2, 100, 10);

	}
}
