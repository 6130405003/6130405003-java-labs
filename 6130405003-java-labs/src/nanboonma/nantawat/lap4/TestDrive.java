/* The program TestDrive get gasoline, speed, max speed,accelerate, model, and color of automobile and display gasoline, speed,max speed,acceleration, model, color and calcurate number of car of 
 * each automobile that get in program.
 * 
 * Author: Nantawat Nanboonma
 * ID: 613040500-3
 * Sec: 2
 * Date: February 8, 2019
 */

package nanboonma.nantawat.lap4;

import nanboonma.nantawat.lap4.Automobile.Color;

/**
 * The program TestDrive get gasoline, speed, max speed,accelerate, model, and color of automobile and display gasoline, speed,max speed,acceleration, model, color and calcurate number of car of 
 * each automobile that get in program.
 * @author �ѹ��Ѳ��
 *
 */
public class TestDrive {
	
	/**
	 * This main will be display operation of accelerate brakes and refuel.
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Automobile car1 = new Automobile(100, 0, 200, 0, "Sport car", Color.RED);
		Automobile car2 = new Automobile(100, 0, 100, 0, "Electric car", Color.WHITE);
		System.out.println("Car 1's max speed is " + car1.getMaxSpeed());
		System.out.println("Car 2's max speed is " + car2.getMaxSpeed());
		car1.setMaxSpeed(280);
		System.out.println("Car 1's max speed has increased to " + car1.getMaxSpeed());
		System.out.println("Threr are " + Automobile.getNumberOfAutomobile() + " automobile");
		Automobile car3 = new Automobile();
		System.out.println("Now there are " + Automobile.getNumberOfAutomobile() + " automobile");
		System.out.println(car1);
		System.out.println(car3);
		
	}

}
