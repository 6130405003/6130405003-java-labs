package nanboonma.nantawat.lap5;
/**
 * This programs get information from SamsungDevice2019 and set information return toString.
 * 
 * Author: Nantawat Nanboonma
 * ID: 613040500-3
 * Sec: 2
 * Date: February 18, 2019
 *
 */
public class SamsungDevice extends MobileDevice implements DisplayTime {

	private static String os = "Android";
	private double androidVersion;
	private static String brand = "Samsung";
	/**
	 * this method will get Brand to read and return Brand.
	 * @return
	 */
	public static String getBrand() {
		return brand;
	}
	/**
	 * this method will set Brand is information that user has given.
	 * @param brand
	 */
	public static void setBrand(String brand) {
		SamsungDevice.brand = brand;
	}
	/**
	 * this method will get AndroidVersion to read and return AndroidVersion.
	 * @return
	 */
	public double getAndroidVersion() {
		return androidVersion;
	}
	/**
	 * this method will set AndroidVersion is information that user has given.
	 * @param androidVersion
	 */
	public void setAndroidVersion(double androidVersion) {
		this.androidVersion = androidVersion;
	}
	/**
	 * this constructors will accept modelName,Price,androidVersion and set information.
	 * @param modelName
	 * @param price
	 * @param androindVersion
	 */
	public SamsungDevice(String modelName, int price, double androindVersion) {
		super(modelName, os, price);

	}
	/**
	 * this constructors will accept modelName,Price,weight,androidVersion and set information.
	 * @param modelName
	 * @param price
	 * @param weight
	 * @param androindVersion
	 */
	public SamsungDevice(String modelName, int price, int weight, double androindVersion) {
		super(modelName, os, price, weight);
		this.setAndroidVersion(androindVersion);

	}
	/**
	 * this method will return each information form any Device to String.
	 */
	public String toString() {
		return ("MobileDevice [Model] name:" + getModelName() + ", OS: " + os + ", Price:" + getPrice()
				+ " Baht, Weight:" + getWeight() + " g." + " Version: " + getAndroidVersion() + "]");
	}
	/**
	 * this method will display string to see.
	 */
	public void displayTime() {
		System.out.println("Display times in both using a digital format and using an analog watch");
		
	}
}
