package nanboonma.nantawat.lab9;

import nanboonma.nantawat.lab8.MyBall;
/**
 * this class extends from MyBall and this will be class for draw or
 * fill circle and set Velocity of ball.
 * 
 * Author: Nantawat Nanboonma ID: 613040500-3 Sec: 2 Date: April 7, 2019
 * 
 * @author �ѹ��Ѳ��
 *
 */
public class MyBallV2 extends MyBall {
	protected int ballVelX;
	protected int ballVelY;
	/**
	 * this method is made for move ball.
	 */
	public void move() {
		super.x += ballVelX;
		super.y += ballVelY;
	}
	/**
	 * this constructor made for accept two input parameter of x,y and get size of
	 * circle is diameter.
	 * 
	 * @param x
	 * @param y
	 */
	public MyBallV2(double x, double y) {
		super(x, y);

	}

}
