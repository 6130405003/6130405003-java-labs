package nanboonma.nantawat.lab9;

import nanboonma.nantawat.lab8.MyCanvas;
import nanboonma.nantawat.lab8.MyPedal;
/**
 * this class extends from Rectangle2D.Double and this will be class for draw or
 * fill Rectangle.
 * 
 * Author: Nantawat Nanboonma ID: 613040500-3 Sec: 2 Date: April 7, 2019
 * 
 * @author �ѹ��Ѳ��
 *
 */
public class MyPedalV2 extends MyPedal {
	/**
	 * this is variable speed of pedal when user pressed button.
	 */
	protected final static int speedPedal = 40;
	/**
	 * this constructor made for accept two input parameter of x,y and get width and height of
	 * rectangle is pedalWidth and pedalHeight.
	 * 
	 * @param x
	 * @param y
	 */
	public MyPedalV2(int x, int y) {
		super(x, y);
		// TODO Auto-generated constructor stub
	}
	/**
	 * this method is made for move pedal to left side.
	 */
	public void moveLeft() {
		x -= speedPedal;
		if (x - speedPedal < 0) {
			x = 0;
		} else {

		}
	}
	/**
	 * this method is made for move pedal to right side.
	 */
	public void moveRight() {
		x += speedPedal;
		if (speedPedal + x >= MyCanvas.WIDTH) {
			x = MyCanvas.WIDTH - MyPedal.pedalWigth;

		} else {

		}
	}

}
