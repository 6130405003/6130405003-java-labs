package nanboonma.nantawat.lap5;
/**
 * this programs will display an information each MobileDevice to see.
 * 
 * Author: Nantawat Nanboonma
 * ID: 613040500-3
 * Sec: 2
 * Date: February 18, 2019
 *
 */
public class InterestedMobileDevices {
	/**
	 * this method will proceed an information and run code to see information.
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		MobileDevice galaxyNote9 = new MobileDevice("Galaxy Note 9", "Android", 25500, 201);
		MobileDevice iPadGen6 = new MobileDevice("Apple iPad Mini 3", "iOS", 11500);
		System.out.println(galaxyNote9);
		System.out.println(iPadGen6);
		iPadGen6.setPrice(11000);
		System.out.println(iPadGen6.getModelName() + " has new price as " + iPadGen6.getPrice() + " Baht.");
		System.out.println(galaxyNote9.getModelName() + " has weight as " + galaxyNote9.getWeight() + " grams.");

	}

}
