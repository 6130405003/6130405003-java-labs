package nanboonma.nantawat.lab8;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

/**
 * This program MyCanvasV4 extends from class MyCanvasV3 will draw and fill
 * ball, pedal and 10 brick 7 row.
 *
 * 
 * Author: Nantawat Nanboonma ID: 613040500-3 Sec: 2 Date: March 28, 2019
 * 
 * @author �ѹ��Ѳ��
 *
 */

public class MyCanvasV4 extends MyCanvasV3 {
	/**
	 * this is constructors of circle to draw or fill ball.
	 */
	protected MyBall ballV4;
	/**
	 * this is constructors of rectangle to draw or fill brick.
	 */
	protected MyBrick brickV4;
	/**
	 * this is constructors of rectangle to draw or fill pedal.
	 */
	protected MyPedal pedalV4;
	Color[] color = { Color.RED, new Color(235, 201, 5), Color.YELLOW, Color.GREEN, Color.CYAN, Color.BLUE,
			new Color(251, 0, 207) };

	/**
	 * this method will draw shape Graphics2D
	 */
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.fillRect(0, 0, WIDTH + 100, HEIGHT + 100);
		Graphics2D g2d = (Graphics2D) g;
		brickV4 = new MyBrick(0, 0);
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 7; j++) {
				brickV4.x = 0 + i * brickV4.brickWidth;
				brickV4.y = 100 + j * brickV4.brickHeight;
				g2d.setColor(color[j]);
				g2d.fill(brickV4);

				g2d.setColor(Color.BLACK);
				g2d.setStroke(new BasicStroke(4));
				g2d.draw(brickV4);
			}
			g2d.setColor(Color.GRAY);
			pedalV4 = new MyPedal(400 - (MyPedal.pedalWigth / 2), HEIGHT - pedalV4.pedalHeight);
			g2d.fill(pedalV4);
			g2d.setColor(Color.WHITE);
			ballV4 = new MyBall(400 - (MyBall.diameter / 2), HEIGHT - pedalV4.pedalHeight - ballV4.diameter);

			g2d.fill(ballV4);

		}
	}

}
