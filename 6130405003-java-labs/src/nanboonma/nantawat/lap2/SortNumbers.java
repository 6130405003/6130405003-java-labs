/*
 * A program called SortNumbers that accept five decimals, read and store those numbers in an array, 
 * and then use Java subroutine to sort these numbers
 * 
* Author: Nantawat Nanboonma
* ID: 613040500-3
* Sec: 2
* Date: January 21, 2019
*
 */


package nanboonma.nantawat.lap2;

import java.util.Arrays;

public class SortNumbers {

	public static void main(String[] args) {
		if (args.length != 5) {
			System.err.println("SortNumbers <argument1> <argument2> <argument3> <argument4> <argument5>");
		}else {
			float args1 = Float.parseFloat(args[0]);
			float args2 = Float.parseFloat(args[1]);
			float args3 = Float.parseFloat(args[2]);;
			float args4 = Float.parseFloat(args[3]);
			float args5 = Float.parseFloat(args[4]);
			float [] sorted = {args1,args2,args3,args4,args5};
			
			Arrays.sort(sorted);
			for (float i : sorted) {
				System.out.print(i + " ");
			}
		}
	}

}
