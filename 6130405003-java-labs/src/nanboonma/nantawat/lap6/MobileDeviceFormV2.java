package nanboonma.nantawat.lap6;

import java.awt.*;
import javax.swing.*;

/**
 * This program MobileDeviceFormV2 extends from class MobileDeviceFormV1 will
 * display like MobileDeviceFormV2 and have ComboBox of device and textArea of
 * review.
 * 
 * Author: Nantawat Nanboonma ID: 613040500-3 Sec: 2 Date: February 27, 2019
 * 
 * @author �ѹ��Ѳ��
 *
 */
public class MobileDeviceFormV2 extends MobileDeviceFormV1 {
	/**
	 * this is a panel of review.
	 */
	protected JPanel reviewPanel;
	/**
	 * this is a label of type of device.
	 */
	protected JLabel Type;
	/**
	 * this comboBox of device
	 */
	protected JComboBox device;
	/**
	 * this is a label of review.
	 */
	protected JLabel review;
	/**
	 * this is a label of review text.
	 */
	protected JTextArea txtArea;
	/**
	 * this is a scroll pane for textArea
	 */
	protected JScrollPane scroll;

	/**
	 * this is a constructor that super title from MobileDeviceFormV1 and tranfrom
	 * class to be like MobileDeviceFormV1.
	 * 
	 * @param title
	 */
	public MobileDeviceFormV2(String title) {
		super(title);

	}

	/**
	 * this method is the operation like MobileDeviceFormV1 and just get ComboBox
	 * and TextArea.
	 */
	protected void addComponents() {
		super.addComponents();
		reviewPanel = new JPanel();
		reviewPanel.setLayout(new GridLayout(0, 1));
		Type = new JLabel("Type: ");
		device = new JComboBox();
		device.addItem("Phone");
		device.addItem("Tablet");
		device.addItem("Smart TV");
		device.setEditable(true);
		review = new JLabel("Review: ");
		txtArea = new JTextArea(3, 35);
		txtArea.setLineWrap(true);
		txtArea.setWrapStyleWord(true);
		txtArea.setRows(2);
		txtArea.setText(
				"Bigger than previous Note phones in every way, the Samsung Galaxy Note 9 has a larger 6.4-inch screen, "
						+ "heftier 4,000mAh battery, and a massive 1TB of storage option. ");
		JScrollPane scroll = new JScrollPane(txtArea);
		scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		reviewPanel.setLayout(new BorderLayout());
		reviewPanel.add(review, BorderLayout.CENTER);
		reviewPanel.add(scroll, BorderLayout.SOUTH);

		panelGrid.add(Type, BorderLayout.WEST);
		panelGrid.add(device, BorderLayout.EAST);
		panel.add(reviewPanel, BorderLayout.CENTER);

	}

	/**
	 * this method is the method that run JFrame process code.
	 */
	public static void createAndShowGUI() {
		MobileDeviceFormV2 mobileDeviceFormV2 = new MobileDeviceFormV2("Mobile Device Form V2");
		mobileDeviceFormV2.addComponents();
		mobileDeviceFormV2.setFrameFeatures();

	}

	/**
	 * this main will display a result from code.
	 * 
	 * @param args
	 */

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}
