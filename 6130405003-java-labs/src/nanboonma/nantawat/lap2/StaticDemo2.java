package nanboonma.nantawat.lap2;
import java.util.*;
/**
 * This program is to demo about static methods, overloading methods
 * and class import
 * 
 * @author admin
 * @version
 */
public class StaticDemo2 {
	static int add(int n1,int n2) {
		int result = n1 + n2;
		return result;
		

	}
	static int add(int n1, int n2, int n3) {
		int result = n1 + n2 + n3;
		return result;
	}
	
	public static void main(String[] args) {
		int n1, n2;
		
		Scanner scanner = new Scanner(System.in);
		Random random = new Random();
		int n3 = random.nextInt(11);
		System.out.println("Enter two integers:");
		n1 = scanner.nextInt();
		n2 = scanner.nextInt();
		
		int result = add(n1,n2,n3);
		System.out.println(n1+ " + "+ n2 +" + " + n3 + " = " + result);
		scanner.close();
	}

}
