package nanboonma.nantawat.lab9;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.geom.Rectangle2D;
import java.util.Random;

import nanboonma.nantawat.lab8.MyBall;
import nanboonma.nantawat.lab8.MyBrick;
import nanboonma.nantawat.lab8.MyCanvas;
import nanboonma.nantawat.lab8.MyPedal;
/**
 * This program MyCanvasV8 extends from class MyCanvasV7 will draw ball
 * and ball can run in panel
 *
 * 
 * Author: Nantawat Nanboonma ID: 613040500-3 Sec: 2 Date: April 7, 2019
 * 
 * @author �ѹ��Ѳ��
 *
 */
public class MyCanvasV8 extends MyCanvasV7 implements Runnable, KeyListener {
	/**
	 * this variable number of color.
	 */
	protected int numCol = MyCanvas.WIDTH / MyBrick.brickWidth;
	/**
	 * this variable number of brick row.
	 */
	protected int numRow = 7;
	/**
	 * this variable number of all brick.
	 */
	protected int numVisibleBricks = numCol * numRow;
	/**
	 * this is array of rectangle to draw or fill bricks.
	 */
	protected MyBrickV2[][] bricks;
	/**
	 * this is list of color to set color of bricks.
	 */
	protected Color[] color = { Color.MAGENTA, Color.BLUE, Color.CYAN, Color.GREEN, Color.YELLOW, Color.ORANGE,
			Color.RED };
	/**
	 * this is constructors of rectangle to draw or fill pedal.
	 */
	protected MyPedalV2 pedal;
	/**
	 * this is variable of livee in game.
	 */
	protected int lives;
	/**
	 * this is constructors of MyCavasV8 to set Velocity of ball and run ball and set location to draw and fill brick.
	 */
	public MyCanvasV8() {
		bricks = new MyBrickV2[numRow][numCol];
		ball = new MyBallV2(MyCanvas.WIDTH / 2 - MyBall.diameter / 2,
				MyCanvas.HEIGHT - MyBall.diameter - MyPedal.pedalHeight);
		ball.ballVelX = 0;
		ball.ballVelY = 0;

		running = new Thread(this);
		for (int i = 0; i < numRow; i++) {
			for (int j = 0; j < numCol; j++) {
				bricks[i][j] = new MyBrickV2(j * MyBrick.brickWidth, super.HEIGHT / 3 - i * MyBrick.brickHeight);

			}
		}
		running.start();
		setFocusable(true);
		addKeyListener(this);

		pedal = new MyPedalV2(MyCanvas.WIDTH / 2 - MyPedal.pedalWigth / 2, MyCanvas.HEIGHT - MyPedal.pedalHeight);
		lives = 3;
	}
	/**
	 * this method will draw shape Graphics2D.
	 */
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setColor(Color.BLACK);
		g2d.fill(new Rectangle(0, 0, super.WIDTH, super.HEIGHT));

		for (int i = 0; i < numRow; i++) {
			for (int j = 0; j < numCol; j++) {
				if (bricks[i][j].visible) {
					g2d.setColor(color[i]);
					g2d.fill(bricks[i][j]);
					g2d.setStroke(new BasicStroke(4));
					g2d.setColor(Color.BLACK);
					g2d.draw(bricks[i][j]);
				}
			}
		}
		g2d.setColor(Color.WHITE);
		g2d.fill(ball);

		g2d.setColor(Color.GRAY);
		g2d.fill(pedal);

		g2d.setFont(new Font("SanSerif", Font.BOLD, 20));
		g2d.setColor(Color.BLUE);
		String s = "Live " + lives;
		g2d.drawString(s, 10, 30);

		if (numVisibleBricks == 0) {
			g2d.setFont(new Font("SanSerif", Font.BOLD, 100));
			g2d.setColor(Color.GREEN);
			String won = "YOU WON";
			g2d.drawString(won, super.WIDTH / 7, super.HEIGHT / 2);

		}
		if (lives == 0) {
			g2d.setFont(new Font("SanSerif", Font.BOLD, 100));
			g2d.setColor(Color.GRAY);
			String over = "GAME OVER";
			g2d.drawString(over, super.WIDTH / 9, super.HEIGHT / 2);

		}

	}
	/**
	 * this method is made for write code for ball to run.
	 */
	public void run() {
		while (true) {
			if (ball.x + MyBall.diameter >= super.WIDTH) {
				ball.x = super.WIDTH - MyBall.diameter;
				ball.ballVelX *= -1;

			} else if (ball.x + MyBall.diameter <= 30) {
				ball.x = 0;
				ball.ballVelX *= -1;

			} else if (ball.y + MyBall.diameter >= super.HEIGHT) {
				ball.y = super.HEIGHT - MyBall.diameter;
				ball.ballVelY *= -1;

			} else if (ball.y + MyBall.diameter <= 30) {
				ball.y = 0;
				ball.ballVelY *= -1;

			}
			for (int i = 0; i < numRow; i++) {
				for (int j = 0; j < numCol; j++) {
					if (bricks[i][j].visible) {
						checkCollision1(ball, bricks[i][j]);
					}
				}

			}
			checkPassBottom();
			collideWithPedal(ball, pedal);
			if (numVisibleBricks == 0) {
				break;

			}
			if (lives == 0) {
				break;

			}

			ball.move();
			repaint();

			try {
				Thread.sleep(20);
			} catch (InterruptedException ex) {
			}

		}
	}
	/**
	 * this method is made for check if ball that is greater than HEIGHT.
	 * @param ball
	 * @param brick
	 */
	private void checkPassBottom() {
		if (ball.y + MyBall.diameter >= MyCanvas.HEIGHT) {
			ball.x = pedal.x + MyPedal.pedalWigth / 2 - MyBall.diameter / 2;
			ball.y = MyCanvas.HEIGHT - MyBall.diameter - MyPedal.pedalHeight;
			ball.ballVelX = 0;
			ball.ballVelY = 0;
			lives--;
			repaint();
		}
	}
	/**
	 * this method is made for check ball that is greater than WIDTH.
	 * @param ball
	 * @param brick
	 */
	public void checkCollision1(MyBallV2 ball, MyBrickV2 bricks) {
		double x = ball.x + MyBall.diameter / 2;
		double y = ball.y + MyBall.diameter / 2;
		double deltaX = x - Math.max(bricks.x, Math.min(x, bricks.x + MyBrick.brickWidth));
		double deltaY = y - Math.max(bricks.y, Math.min(y, bricks.y + MyBrick.brickHeight));
		boolean collided = (deltaX * deltaX + deltaY * deltaY) < (MyBall.diameter * MyBall.diameter) / 4.0;
		if (collided) {
			if (deltaX * deltaX < deltaY * deltaY) {
				ball.ballVelY *= -1;

			} else {
				ball.ballVelX *= -1;
			}
			numVisibleBricks--;
			ball.move();
			bricks.visible = false;
		}
	}
	/**
	 * this method is made for check if ball hit the pedal.
	 * @param ball
	 * @param pedal
	 */
	private void collideWithPedal(MyBallV2 ball, MyPedalV2 pedal) {
		double x = ball.x + MyBall.diameter / 2.0;
		double y = ball.y + MyBall.diameter / 2.0;
		double deltaX = x - Math.max(pedal.x, Math.min(x, pedal.x + MyPedalV2.pedalWigth));
		double deltaY = y - Math.max(pedal.y, Math.min(y, pedal.y + MyPedalV2.pedalHeight));

		boolean collided = (deltaX * deltaX + deltaY * deltaY) < (MyBall.diameter * MyBall.diameter) / 4.0;
		if (collided) {
			if (deltaX * deltaX < deltaY * deltaY) {
				ball.ballVelY *= -1;

			} else {
				ball.ballVelX *= -1;
			}
			ball.move();
		}

	}

	@Override
	public void keyPressed(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_LEFT) {
			pedal.moveLeft();
		} else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
			pedal.moveRight();

		} else if (e.getKeyCode() == KeyEvent.VK_SPACE) {
			ball.ballVelY = 4;
			Random ran = new Random();
			int result = ran.nextInt(2);
			int velX = 0;
			if (result == 0) {
				velX = -4;
			} else if (result == 1) {
				velX = 4;
			}
			ball.ballVelX = velX;
		}

	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub

	}

}
