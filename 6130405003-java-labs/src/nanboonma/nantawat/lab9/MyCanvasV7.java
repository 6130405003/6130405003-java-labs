package nanboonma.nantawat.lab9;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Arrays;

import nanboonma.nantawat.lab8.MyBall;
import nanboonma.nantawat.lab8.MyBrick;
import nanboonma.nantawat.lab8.MyCanvas;
/**
 * This program MyCanvasV7 extends from class MyCanvasV6 will draw ball
 * and ball can run in panel
 *
 * 
 * Author: Nantawat Nanboonma ID: 613040500-3 Sec: 2 Date: April 7, 2019
 * 
 * @author �ѹ��Ѳ��
 *
 */
public class MyCanvasV7 extends MyCanvasV6 implements Runnable {
	/**
	 * this variable number of brick.
	 */
	protected int numBricks = 10;
	/**
	 * this is constructors of circle to draw or fill ball.
	 */
	protected MyBallV2 ball = new MyBallV2(0, 0);
	/**
	 * this is array of rectangle to draw or fill bricks.
	 */
	protected MyBrickV2 brick[];
	/**
	 * this is constructor of thread to order ball to run.
	 */
	Thread running;
	/**
	 * this is constructors of MyCavasV7 to set Velocity of ball and run ball and set location to draw and fill brick.
	 */
	public MyCanvasV7() {
		Thread running = new Thread(this);
		ball.ballVelX = 2;
		ball.ballVelY = 2;
		brick = new MyBrickV2[numBricks];
		for (int i = 0; i < numBricks; i++) {
			brick[i] = new MyBrickV2(MyBrick.brickWidth * i, super.HEIGHT / 2);
		}
		running.start();
	}
	/**
	 * this method will draw shape Graphics2D.
	 */
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setColor(Color.BLACK);
		g2d.fill(new Rectangle(0, 0, super.WIDTH, super.HEIGHT));
		g2d.setColor(Color.WHITE);
		g2d.fill(ball);
		for (int i = 0; i < numBricks; i++) {
			if (brick[i].visible) {
				g2d.setColor(Color.BLUE);
				g2d.setStroke(new BasicStroke(7));
				g2d.draw(brick[i]);
				g2d.setColor(Color.RED);
				g2d.fill(brick[i]);
			}
		}
	}
	/**
	 * this method is made for write code for ball to run.
	 */
	public void run() {
		while (true) {
			if (ball.x + MyBall.diameter >= super.WIDTH) {
				ball.x = super.WIDTH - MyBall.diameter;
				ball.ballVelX *= -1;

			} else if (ball.x + MyBall.diameter <= 30) {
				ball.x = 0;
				ball.ballVelX *= -1;

			} else if (ball.y + MyBall.diameter >= super.HEIGHT) {
				ball.y = super.HEIGHT - MyBall.diameter;
				ball.ballVelY *= -1;

			} else if (ball.y + MyBall.diameter <= 30) {
				ball.y = 0;
				ball.ballVelY *= -1;

			}
			for (int i = 0; i < numBricks; i++) {
				if (brick[i].visible) {
					checkCollision(ball, brick[i]);
				}
			}
			ball.move();
			repaint();

			try {
				Thread.sleep(20);
			} catch (InterruptedException ex) {
			}

		}
	}
	/**
	 * this method is made for check ball that is greater than WIDTH.
	 * @param ball
	 * @param brick
	 */
	public void checkCollision(MyBallV2 ball, MyBrickV2 brick) {
		double x = ball.x + MyBall.diameter / 2;
		double y = ball.y + MyBall.diameter / 2;
		double deltaX = x - Math.max(brick.x, Math.min(x, brick.x + MyBrick.brickWidth));
		double deltaY = y - Math.max(brick.y, Math.min(y, brick.y + MyBrick.brickHeight));
		boolean collided = (deltaX * deltaX + deltaY * deltaY) < (MyBall.diameter * MyBall.diameter) / 4.0;
		if (collided) {
			if (deltaX * deltaX < deltaY * deltaY) {
				ball.ballVelY *= -1;

			} else {
				ball.ballVelX *= -1;
			}
			ball.move();
			brick.visible = false;
		}
	}
}
