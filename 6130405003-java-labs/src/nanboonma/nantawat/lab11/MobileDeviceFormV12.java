package nanboonma.nantawat.lab11;

import java.awt.event.ActionEvent;
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileSystemView;

import nanboonma.nantawat.lap5.MobileDevice;
import nanboonma.nantawat.lecture.Book;

/**
 * This program MobileDeviceFormV12 extends from class MobileDeviceFormV11
 * program will add listener of save file and open file.
 * 
 * Author: Nantawat Nanboonma ID: 613040500-3 Sec: 2 Date: May 4, 2019
 * 
 * @author �ѹ��Ѳ��
 *
 */
public class MobileDeviceFormV12 extends MobileDeviceFormV11 {
	/**
	 * Declare each variable in program.
	 */
	protected JFileChooser fc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());

	/**
	 * this is constructors of MobileDeviceFormV12 that super code from
	 * MobileDeviceFormV11.
	 */
	public MobileDeviceFormV12(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}

	/**
	 * this method will adListener to object.
	 */
	public void addListeners() {
		super.addListeners();
	}

	/**
	 * this method will get Source when user clicked and progress condition when
	 * user clicked.
	 */
	public void actionPerformed(ActionEvent event) {
		Object src = event.getSource();

		if (src == saveItem) {
			int returnVal = fc.showSaveDialog(null);
			if (returnVal == fc.APPROVE_OPTION) {
				java.io.File selectedFile = fc.getSelectedFile();
				try {
					FileOutputStream fileOutput = new FileOutputStream(selectedFile);
					ObjectOutputStream objectOutput = new ObjectOutputStream(fileOutput);
					for (int i = 0; i < list.size(); i++) {
						objectOutput.writeObject(list.get(i));
					}
					objectOutput.close();
					fileOutput.close();
				} catch (Exception ex) {
					ex.printStackTrace(System.err);
				}
				JOptionPane.showMessageDialog(null, "Saving file " + selectedFile.getName());
			} else if (returnVal == fc.CANCEL_OPTION) {
				JOptionPane.showMessageDialog(null, "Save command cancelled by user");
			}
		} else if (src == openItem) {
			int returnVal = fc.showOpenDialog(null);
			if (returnVal == fc.APPROVE_OPTION) {
				java.io.File selectedFile = fc.getSelectedFile();
				try {
					FileInputStream fileInput = new FileInputStream(selectedFile);
					ObjectInputStream objectInput = new ObjectInputStream(fileInput);
					String showOutput = "";
					try {
						while (true) {
							MobileDevice output = (MobileDevice) objectInput.readObject();
							showOutput += output + "\n";
						}
					} catch (EOFException eof) {
						JOptionPane.showMessageDialog(null, showOutput);
						fileInput.close();
						objectInput.close();
					}
				} catch (IOException ex) {
					ex.printStackTrace();
				} catch (Exception ex2) {
					ex2.printStackTrace();
				}

			} else if (returnVal == fc.CANCEL_OPTION) {
				JOptionPane.showMessageDialog(null, "Open command cancelled by user");
			}

		} else {
			super.actionPerformed(event);
		}
	}

	/**
	 * this method is the method that run JFrame process code.
	 */
	public static void createAndShowGUI() {
		MobileDeviceFormV12 mobileDeviceFormV12 = new MobileDeviceFormV12("Mobile Device Form V12");
		mobileDeviceFormV12.addComponents();
		mobileDeviceFormV12.initComponents();
		mobileDeviceFormV12.addMenus();
		mobileDeviceFormV12.enableKeyboard();
		mobileDeviceFormV12.addListeners();
		mobileDeviceFormV12.setFrameFeatures();
	}

	/**
	 * this main will display a result from code.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}
