/*A program called DataTypes that declare the following variable in the given types and conditions:
	1.A String variable with value = your name (firstname lastname)
	2.A String variable with value = your student ID
	3.A char variable with value = theFirstLetterOfYourFirstName (need to use String subroutine to get the first letter)
	4.A boolean variable with value = true
	5.An int variable with value = theLastTwoDigitsOfYourlIDNumber (declare a variable with an octal value)  For example, for 34 in base 10, you should declare it as  myIDInOctal = 042
	6.An int variable with value = theLastTwoDigitsOfYourIDNumber (declare a variable with a hexadecimal value)
	7.A long variable with value = theLastTwoDigitsOfYourIDNumber (declare a variable with a long value)
	8.A float variable with value = theLastTwoDigitsOfYourIDNumber and has the floating point of theFirstTwoDigitsOfYourIDNumber
	9.A double variable with value = theLastTwoDigitsOfYourIDNumber and has the floating point of theFirstTwoDigitsOfYourIDNumber
	
* Author: Nantawat Nanboonma
* ID: 613040500-3
* Sec: 2
* Date: January 21, 2019
*

 */

package nanboonma.nantawat.lap2;

public class DataTypes {

	public static void main(String[] args) {
		String name = "Nantawat Nanboonma";
		String id = "6130405003";
		char first_digits_name = name.charAt(0);
		boolean whatever = true;
		int inoctal = 003;
		int inhexademical = 0x03;
		int last_two_id = 03;
		float id_first_last_float = Float.parseFloat("03.61");
		double id_first_last_double = 03.61;
		System.out.println("My name is " + name);
		System.out.println("My student name is " + id );
		System.out.println(first_digits_name+" "+ whatever+" "+ inoctal+" " + inhexademical);
		System.out.println(last_two_id+" "+ id_first_last_float+" "+ id_first_last_double);
		
	}

}
