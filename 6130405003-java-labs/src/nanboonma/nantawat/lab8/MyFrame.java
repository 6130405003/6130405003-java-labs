package nanboonma.nantawat.lab8;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

/**
 * This program MyFrame extends from class JFrame will add paintCompenent and
 * show interface.
 * 
 * 
 * Author: Nantawat Nanboonma ID: 613040500-3 Sec: 2 Date: March 28, 2019
 * 
 * @author �ѹ��Ѳ��
 *
 */
public class MyFrame extends JFrame {
	/**
	 * this is a constructor that super text from JFrame and tranform class to be
	 * like JFrame.
	 * 
	 * @param title
	 */
	public MyFrame(String text) {
		super(text);

	}

	/**
	 * this main will display a result from code.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});

	}

	/**
	 * this method is the method that run JFrame process code.
	 */
	public static void createAndShowGUI() {
		MyFrame msw = new MyFrame("My Frame");
		msw.addComponents();
		msw.setFrameFeatures();
	}

	/**
	 * this method will add paintComponent from MyCanvas in JFrame
	 */
	protected void addComponents() {
		add(new MyCanvas());
	}

	/**
	 * this method made for set location of JFrame.
	 */
	protected void setFrameFeatures() {
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}
