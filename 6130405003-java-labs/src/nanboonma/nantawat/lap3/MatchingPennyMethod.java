/*  MatchingPennyMethod the program defines three methods, namely acceptInput(),genComChoice() and displayWiner() 
 described below. The program also contains two static String variable, namely humanChoice and compChoice to be used in 
 the program methods.
 * 
 * Author: Nantawat Nanboonma
 * ID: 613040500-3
 * Sec: 2
 * Date: January 28, 2019
 * 
 */

package nanboonma.nantawat.lap3;
import java.util.*;
/**
 *  MatchingPennyMethod the program defines three methods, namely acceptInput(),genComChoice() and displayWiner() 
 described below. The program also contains two static String variable, namely humanChoice and compChoice to be used in the 
 program methods.
 *
 */
public class MatchingPennyMethod {
	
	
	/** 
	 * Method acceptInput() reads in a choice (head or tail) from a user from Eclipse console using Java class Scanner. 
		If the input is entered incorrectly, an error message will be displayed.
		 */
	static String acceptInput(String lower_tail_head){
		if ((lower_tail_head.equals("head"))) {
			return lower_tail_head;
			
		}	else if (lower_tail_head.equals("tail")) {
			return lower_tail_head;
			
		}	else if (lower_tail_head.equals("exit")){
			System.out.println("Good bye");
			System.exit(0);
		}
		
		else {
			System.err.print("Incorrect input. head or tail only \n");
		} return lower_tail_head;
		
	}
	static int genComChoice(){
		/** Method genComChoice() generate a choice head or tail for a computer using Math.random().
		 */
		int max = 1;
		int min = 0;
		int random_num = min + (int)(Math.random() * ((max - min) + 1));
		return random_num;
	}
	
	/**
	 * Method displayWiner() accepts choices from user and computer and displays user as a winner if the choices are the same, 
	   computer as a winner if the choice are different.
	 */
	static void displayWiner(String result,int random1){
		
		if ((result.equals("head"))&&(random1==0)){
			System.out.println( "You play " + result);
			System.out.println("Computer play head");
			System.out.println("You win \n");
			
		}	else if ((result.equals("tail"))&&(random1==1)){
				System.out.println( "You play " + result);
				System.out.println("Computer play tail");
				System.out.println("You win \n");
			
		}
			else if ((result.equals("tail"))&&(random1==0)){
				System.out.println( "You play " + result);
				System.out.println("Computer play head");
				System.out.println("Computer win \n");
		}
			else if ((result.equals("head"))&&(random1==1)){
				System.out.println( "You play " + result);
				System.out.println("Computer play tail");
				System.out.println("Computer win \n");
		}
	}
	
	/**
	* The program defines three methods, namely acceptInput(), genComChoice() and displayWiner() described below. The program also 
	* contains two static String variable, namely humanChoice and compChoice to be used in the program methods. The program 
	* repeatedly run the game until the user enter the choice �exit�.
	* @Author: Nantawat Nanboonma
	* @Version : 11.0.1 
	*/
	public static void main(String[] args) {
		while (true) {
			Scanner input_text = new Scanner(System.in);
			System.out.print("Enter head or tail: ");
			String text_input = input_text.next();
			String lower_tail_head = text_input.toLowerCase();
			String real_result = acceptInput(lower_tail_head);
			int random1 = genComChoice();
			displayWiner(real_result,random1);
		}
		
	}

}
