package nanboonma.nantawat.lab11;

import java.awt.event.ActionEvent;
import java.awt.font.NumericShaper.Range;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

/**
 * This program MobileDeviceFormV11 extends from class MobileDeviceFormV10
 * program will check exception when user when model name is empty and weight is
 * less than 100 and more than 3000.
 * 
 * Author: Nantawat Nanboonma ID: 613040500-3 Sec: 2 Date: April 22, 2019
 * 
 * @author �ѹ��Ѳ��
 *
 */
public class MobileDeviceFormV11 extends MobileDeviceFormV10 {
	/**
	 * Declare each variable in program.
	 */
	protected int MIN_WEIGHT;
	protected int MAX_WEIGHT;

	/**
	 * this is constructors of MobileDeviceFormV11 that super code from
	 * MobileDeviceFormV10.
	 */
	public MobileDeviceFormV11(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}

	/**
	 * this method will adListener to object.
	 */
	public void addListeners() {
		super.addListeners();
	}

	/**
	 * this method will get Source when user clicked and progress condition when
	 * user clicked.
	 */
	public void actionPerformed(ActionEvent event) throws NumberFormatException {
		Object src = event.getSource();

		if (src == okButton) {
			int weightOut = checkEX();
			if (secondTxtField.getText().length() > 0 && weightOut >= MIN_WEIGHT && weightOut <= MAX_WEIGHT) {
				handleOKButton();
			}

		} else {
			super.actionPerformed(event);
		}
	}

	/**
	 * this method will check exception of error
	 * 
	 * @return
	 */
	public int checkEX() {
		int weightOut = 0;
		MIN_WEIGHT = 100;
		MAX_WEIGHT = 3000;
		try {
			if (secondTxtField.getText().isBlank()) {
				throw new Exception();
			}
		} catch (Exception ex) {
			JOptionPane.showMessageDialog(null, "Please enter model name");
		}

		try {
			int weight = Integer.parseInt(thirdTxtField.getText());
			weightOut = weight;
			if (weightOut < MIN_WEIGHT) {
				JOptionPane.showMessageDialog(null,
						"Too light: valid weight is [" + MIN_WEIGHT + "," + MAX_WEIGHT + "]");
			} else if (weightOut > MAX_WEIGHT) {
				JOptionPane.showMessageDialog(null,
						"Too heavy: valid weight is [" + MIN_WEIGHT + "," + MAX_WEIGHT + "]");
			}

		} catch (NumberFormatException ex) {
			JOptionPane.showMessageDialog(null, "Please enter only numeric input for weight");
		}
		return weightOut;
	}

	/**
	 * this method is condition to showMessageDialog when user clicked OKButton.
	 */
	public void handleOKButton() {
		super.handleOKButton();
	}

	/**
	 * this method is the method that run JFrame process code.
	 */
	public static void createAndShowGUI() {
		MobileDeviceFormV11 mobileDeviceFormV11 = new MobileDeviceFormV11("Mobile Device Form V11");
		mobileDeviceFormV11.addComponents();
		mobileDeviceFormV11.initComponents();
		mobileDeviceFormV11.addMenus();
		mobileDeviceFormV11.enableKeyboard();
		mobileDeviceFormV11.addListeners();
		mobileDeviceFormV11.setFrameFeatures();
	}

	/**
	 * this main will display a result from code.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}
