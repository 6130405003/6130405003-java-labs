package nanboonma.nantawat.lab11;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import nanboonma.nantawat.lab10.MobileDeviceFormV9;
import nanboonma.nantawat.lap5.MobileDevice;

/**
 * this class is made for compare price of each mobile device.
 * 
 * @author �ѹ��Ѳ��
 *
 */
class PriceMobile implements Comparator<MobileDevice> {

	@Override
	public int compare(MobileDevice a, MobileDevice b) {
		return a.getPrice() < b.getPrice() ? -1 : a.getPrice() == b.getPrice() ? 0 : 1;
	}
}

/**
 * This program MobileDeviceFormV10 extends from class MobileDeviceFormV9
 * program will add menu Data that include with display, sort, search and
 * remove.
 * 
 * The program will get input information from user and insert information to
 * MobileDevice from lab5 and add toString of MobileDevice to ArrayList.
 * 
 * And program will display,sort price,search and remove device when user click
 * that menu and input model name for search or remove. Author: Nantawat
 * Nanboonma ID: 613040500-3 Sec: 2 Date: April 22, 2019
 * 
 * @author �ѹ��Ѳ��
 *
 */

public class MobileDeviceFormV10 extends MobileDeviceFormV9 {
	/**
	 * Declare each variable in program.
	 */
	protected JMenu dataMenu;
	protected JMenuItem display;
	protected JMenuItem sort;
	protected JMenuItem search;
	protected JMenuItem remove;
	ArrayList<MobileDevice> list = new ArrayList<MobileDevice>();

	/**
	 * this is constructors of MobileDeviceFormV10 that super code from
	 * MobileDeviceFormV9.
	 */
	public MobileDeviceFormV10(String title) {
		super(title);
	}

	/**
	 * this method will adListener to object.
	 */
	public void addListeners() {
		super.addListeners();
		display.addActionListener(this);
		sort.addActionListener(this);
		search.addActionListener(this);
		remove.addActionListener(this);
	}

	/**
	 * this method will get Source when user clicked and progress condition when
	 * user clicked.
	 */
	public void actionPerformed(ActionEvent event) {
		Object src = event.getSource();

		if (src == okButton) {
			handleOKButton();

		} else {
			super.actionPerformed(event);
		}

		if (src == display) {
			displayMessage();

		} else if (src == sort) {
			Collections.sort(list, new PriceMobile());
			displayMessage();
		} else if (src == search) {
			Object searchModel = JOptionPane.showInputDialog(null, "Please input model name to search: ");
			int numSearch = displaySearch(searchModel);
			if (numSearch == -1) {
				JOptionPane.showMessageDialog(null, searchModel + " is NOT found");
			} else {
				JOptionPane.showMessageDialog(null, list.get(numSearch));
			}
		} else if (src == remove) {
			Object searchModel = JOptionPane.showInputDialog(null, "Please input model name to move: ");
			int numRemove = displayRemove(searchModel);
			if (numRemove == -1) {
				JOptionPane.showMessageDialog(null, searchModel + " is NOT found");
			} else {
				JOptionPane.showMessageDialog(null, list.get(numRemove) + " is remove.");
				list.remove(numRemove);
			}
		}
	}

	/**
	 * this method will return number of list that equal search model name.
	 * 
	 * @param searchModel
	 * @return
	 */
	public int displaySearch(Object searchModel) {
		for (int i = 0; i < list.size(); i++) {
			if (list.get(i).getModelName().equals(searchModel)) {
				return i;
			}
		}
		return -1;
	}

	/**
	 * this method will return number of list that equal remove model name.
	 * 
	 * @param searchModel
	 * @return
	 */
	public int displayRemove(Object searchModel) {
		for (int i = 0; i < list.size(); i++) {
			if (list.get(i).getModelName().equals(searchModel)) {
				return i;
			}
		}
		return -1;
	}

	/**
	 * this method will display all of MobileDevice.
	 * 
	 * @param searchModel
	 * @return
	 */
	public void displayMessage() {
		String show = "";
		for (int i = 0; i < list.size(); i++) {
			show += i + 1 + ": " + list.get(i) + "\n\n";
		}
		JOptionPane.showMessageDialog(null, show);
	}

	/**
	 * this method will add information in MobileDevice.
	 * 
	 * @param searchModel
	 * @return
	 */
	public void addMobileDevice() {
		int price = Integer.parseInt(fourthTxtField.getText());
		int weight = Integer.parseInt(thirdTxtField.getText());
		if (androidOs.getSelectedObjects() != null) {
			MobileDevice mobileDevice = new MobileDevice(secondTxtField.getText(), "Android", price, weight);
			list.add(mobileDevice);
			System.out.println(list);
		} else if (ios.getSelectedObjects() != null) {
			MobileDevice mobileDevice = new MobileDevice(secondTxtField.getText(), "iOS", price, weight);
			list.add(mobileDevice);
			System.out.println(list);
		}
	}

	/**
	 * this method is condition to showMessageDialog when user clicked OKButton.
	 */
	public void handleOKButton() {
		super.handleOKButton();
		addMobileDevice();
	}

	/**
	 * this method will add menu in menu bar.
	 */
	protected void addMenus() {
		super.addMenus();
		dataMenu = new JMenu("Data");
		display = new JMenuItem("Display");
		sort = new JMenuItem("Sort");
		search = new JMenuItem("Search");
		remove = new JMenuItem("Remove");
		dataMenu.add(display);
		dataMenu.add(sort);
		dataMenu.add(search);
		dataMenu.add(remove);
		menuBar.add(dataMenu);

	}

	/**
	 * this method is the method that run JFrame process code.
	 */
	public static void createAndShowGUI() {
		MobileDeviceFormV10 mobileDeviceFormV10 = new MobileDeviceFormV10("Mobile Device Form V10");
		mobileDeviceFormV10.addComponents();
		mobileDeviceFormV10.initComponents();
		mobileDeviceFormV10.addMenus();
		mobileDeviceFormV10.enableKeyboard();
		mobileDeviceFormV10.addListeners();
		mobileDeviceFormV10.setFrameFeatures();

	}

	/**
	 * this main will display a result from code.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}
