package nanboonma.nantawat.lab7;

import javax.imageio.ImageIO;
import javax.print.DocFlavor.URL;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
/**
 * This program MobileDeviceFormV6 extends from class MobileDeviceFormV6 will
 * display like MobileDeviceFormV5 and just readImage and add Image in JPanel.
 * 
 * Author: Nantawat Nanboonma ID: 613040500-3 Sec: 2 Date: March 5, 2019
 * 
 * @author �ѹ��Ѳ��
 *
 */

public class MobileDeviceFormV6 extends MobileDeviceFormV5 {
	/**
	 * this this a variable of ReadImage
	 */
	protected ReadImage content;
	/**
	 * this is a panel of V6 was made for get image
	 */
	protected JPanel panelV6;
	public MobileDeviceFormV6(String title) {
		super(title);

	}
	/**
	 * this method will add in JPanel.
	 */
	protected void addComponents() {
		super.addComponents();
		
		panel.remove(reviewPanel);
		panelV6 = new JPanel();
		panelV6.setLayout(new GridLayout(0,1));
		content = new ReadImage();
		panelV6.setLayout(new BorderLayout());
		panelV6.add(reviewPanel,BorderLayout.NORTH);
		panelV6.add(content);
		panel.add(panelV6);
		
	}
	/**
	 * this method is the method that run JFrame process code.
	 */
	public static void createAndShowGUI() {
		MobileDeviceFormV6 mobileDeviceFormV6 = new MobileDeviceFormV6("Mobile Device Form V6");
		mobileDeviceFormV6.addComponents();
		mobileDeviceFormV6.initComponents();
		mobileDeviceFormV6.addMenus();
		mobileDeviceFormV6.setFrameFeatures();

	}

	/**
	 * this main will display a result from code.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
	/**
	 * this class was made to read image file in folder image  and set size of image.
	 * @author �ѹ��Ѳ��
	 *
	 */
	class ReadImage extends JPanel {
		BufferedImage img;
		String filename = "images/galaxyNote9.jpg";
		public void paintComponent(Graphics g) {
			g.drawImage(img,0,0,null);
		}
		public ReadImage() {
			try {
				
				img = ImageIO.read(new File(filename));
			} catch (IOException e) {
				e.printStackTrace(System.err);
			}
		}
		public Dimension getPreferredSize() {
			if (img == null)
				return new Dimension(100,100);
			else 
				return new Dimension(img.getWidth()+20,
						img.getHeight()+60);
		}
		}
}

