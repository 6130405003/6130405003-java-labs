package nanboonma.nantawat.lap5;
/**
 * This programs will display the information of AndroidDevice and AndroidSmartWatch
 * 
 * Author: Nantawat Nanboonma
 * ID: 613040500-3
 * Sec: 2
 * Date: February 18, 2019
 *
 */
public class AndroidDevice2019  {
	/**
	 * This method will proceed information each device and display information.
	 * @param args
	 */
	public static void main(String[] args) {
		AndroidDevice ticwatchPro = new AndroidSmartWatch("Mobvoi", "TicWatch Pro", 8390);
		AndroidSmartWatch xiaomiMiBand3 = new AndroidSmartWatch("Xiaomi", "Mi Band 3", 950);
		ticwatchPro.usage();
		System.out.println(ticwatchPro);
		System.out.println(xiaomiMiBand3);
	}

}
