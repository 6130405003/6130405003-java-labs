package nanboonma.nantawat.lab8;
import java.awt.geom.Ellipse2D;

/**
 * this class extends from Ellipse2D.Double and this will be class for draw or
 * fill circle.
 * 
 * Author: Nantawat Nanboonma ID: 613040500-3 Sec: 2 Date: March 28, 2019
 * 
 * @author �ѹ��Ѳ��
 *
 */
public class MyBall extends Ellipse2D.Double {
	/**
	 * this is the variable of diameter for fill circle.
	 */
	public final static int diameter = 30;

	/**
	 * this constructor made for accept two input parameter of x,y and get size of
	 * circle is diameter.
	 * 
	 * @param x
	 * @param y
	 */
	public MyBall(double x, double y) {
		super(x, y, diameter, diameter);

	}
}
