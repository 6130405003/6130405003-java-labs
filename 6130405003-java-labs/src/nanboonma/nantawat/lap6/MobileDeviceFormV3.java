package nanboonma.nantawat.lap6;

import java.awt.*;
import javax.swing.*;

/**
 * This program MobileDeviceFormV3 extends from class MobileDeviceFormV2 will
 * display like MobileDeviceFormV2 and just have Label and textArea of features
 * and have menuBar.
 * 
 * Author: Nantawat Nanboonma ID: 613040500-3 Sec: 2 Date: February 27, 2019
 * 
 * @author �ѹ��Ѳ��
 *
 */
public class MobileDeviceFormV3 extends MobileDeviceFormV2 {
	/**
	 * this is a label of features.
	 */
	protected JLabel featuresLabel;
	/**
	 * this is a sub panel for get featurePanel and review.
	 */
	protected JPanel newSubPanel;
	/**
	 * this is a panel of features.
	 */
	protected JPanel featurePanel;
	/**
	 * this is a textArea of features.
	 */
	protected JTextArea txtFeatures;
	/**
	 * thi is a menuBar.
	 */
	protected JMenuBar menuBar;
	/**
	 * this is a File menu.
	 */
	protected JMenu menuFile;
	/**
	 * this is a config Menu
	 */
	protected JMenu menuConfig;
	/**
	 * this is a menu Item of File menu.
	 */
	protected JMenuItem newItem;
	/**
	 * this is a menu Item of File menu.
	 */
	protected JMenuItem openItem;
	/**
	 * this is a menu Item of File menu.
	 */
	protected JMenuItem saveItem;
	/**
	 * this is a menu Item of File menu.
	 */
	protected JMenuItem exitItem;
	/**
	 * this is a menu Item of config menu.
	 */
	protected JMenuItem colorItem;
	/**
	 * this is a menu Item of config menu.
	 */
	protected JMenuItem sizeItem;
	/**
	 * this is a features list.
	 */
	protected String featuresList[] = { "Design and build quality", "Great Camera", "Screen", "Battery Life" };
	/**
	 * this is a JList of features
	 */
	protected JList features;

	/**
	 * this is a constructor that super title from MobileDeviceFormV2 and tranfrom
	 * class to be like MobileDeviceFormV2.
	 * 
	 * @param title
	 */
	public MobileDeviceFormV3(String title) {
		super(title);

	}

	/**
	 * this method is the operation like MobileDeviceFormV2 and just get features
	 * object.
	 */
	protected void addComponents() {
		super.addComponents();
		featurePanel = new JPanel();
		featurePanel.setLayout(new GridLayout(0, 2, 1, 10));
		newSubPanel = new JPanel();
		newSubPanel.setLayout(new GridLayout(0, 1));
		featuresLabel = new JLabel("Features: ");
		features = new JList(featuresList);
		featurePanel.add(featuresLabel, BorderLayout.WEST);
		featurePanel.add(features, BorderLayout.EAST);
		reviewPanel.add(featurePanel, BorderLayout.NORTH);

	}

	/**
	 * this method is a operation to take all structure of menu in menuBar.
	 */
	protected void addMenus() {
		menuBar = new JMenuBar();
		menuFile = new JMenu("File");
		menuFile.setLayout(new GridLayout());
		menuConfig = new JMenu("Config");
		newItem = new JMenuItem("New");
		openItem = new JMenuItem("Open");
		saveItem = new JMenuItem("Save");
		exitItem = new JMenuItem("Exit");
		menuFile.add(newItem);
		menuFile.add(openItem);
		menuFile.add(saveItem);
		menuFile.add(exitItem);
		colorItem = new JMenuItem("Color");
		sizeItem = new JMenuItem("Size");
		menuConfig.add(colorItem);
		menuConfig.add(sizeItem);
		menuBar.add(menuFile);
		menuBar.add(menuConfig);
		setJMenuBar(menuBar);

	}

	/**
	 * this method is the method that run JFrame process code.
	 */
	public static void createAndShowGUI() {
		MobileDeviceFormV3 mobileDeviceFormV3 = new MobileDeviceFormV3("Mobile Device Form V3");
		mobileDeviceFormV3.addComponents();
		mobileDeviceFormV3.addMenus();
		mobileDeviceFormV3.setFrameFeatures();

	}

	/**
	 * this main will display a result from code.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}
