package nanboonma.nantawat.lab9;

import nanboonma.nantawat.lab8.MyBrick;
/**
 * this class extends from Rectangle2D.Double and this will be class for draw or
 * fill Rectangle.
 * 
 * Author: Nantawat Nanboonma ID: 613040500-3 Sec: 2 Date: April 7, 2019
 * 
 * @author �ѹ��Ѳ��
 *
 */
public class MyBrickV2 extends MyBrick {
	/**
	 * this boolean is made for show visible of bricks.
	 */
	protected boolean visible = true;
	/**
	 * this constructor made for accept two input parameter of x,y and get width and height of
	 * rectangle is brickWidth and brickHeight.
	 * 
	 * @param x
	 * @param y
	 */
	public MyBrickV2(int x, int y) {
		super(x, y);

	}

}
