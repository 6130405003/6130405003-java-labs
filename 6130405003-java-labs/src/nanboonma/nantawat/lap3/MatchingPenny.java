/*  MatchingPenny that simulates a coin tossing game called matching penny. The program reads in a choice 
(Head or Tail) from a user then compares that choice with a randomly generated choice from a computer. 
If the choices are the same, the user wins. If the choices are different, the computer wins. 

 * Author: Nantawat Nanboonma
 * ID: 613040500-3
 * Sec: 2
 * Date: January 28, 2019
 * 
 */
package nanboonma.nantawat.lap3;
import java.util.*;
/**
 * The program reads in a choice (Head or Tail) from a user then compares that choice with a randomly generated choice from 
 * a computer. If the choices are the same, the user wins. If the choices are different, the computer wins.
 */
public class MatchingPenny {
	/** 
	 * The program reads in a choice (Head or Tail) from a user then compares that choice with a randomly generated choice from 
	 * a computer. If the choices are the same, the user wins. If the choices are different, the computer wins.
	 * @Author: Nantawat Nanboonma
	 * @Version : 11.0.1 
	 */
	public static void main(String[] args) {
		while (true) {
			int max = 1;
			int min = 0;
			int random_num = min + (int)(Math.random() * ((max - min) + 1));
			
			
			Scanner input = new Scanner(System.in);
			System.out.print("Enter head or tail: ");
			String text_input = input.next();
			String lower_tail_head = text_input.toLowerCase();
			
			if ((lower_tail_head.equals("head"))&&(random_num==0)){
				System.out.println( "You play " + lower_tail_head);
				System.out.println("Computer play head");
				System.out.println("You win \n");
			
				
			}	else if ((lower_tail_head.equals("tail"))&&(random_num==1)){
					System.out.println( "You play " + lower_tail_head);
					System.out.println("Computer play tail");
					System.out.println("You win \n");
				
			}
				else if ((lower_tail_head.equals("tail"))&&(random_num==0)){
					System.out.println( "You play " + lower_tail_head);
					System.out.println("Computer play head");
					System.out.println("Computer win \n");
			}
				else if ((lower_tail_head.equals("head"))&&(random_num==1)){
					System.out.println( "You play " + lower_tail_head);
					System.out.println("Computer play tail");
					System.out.println("Computer win \n");
					
			}	else if (lower_tail_head.equals("exit")) {
					System.out.println("Good bye");
					System.exit(0);
					
			}	else {
					System.err.println("Incorrect input. head or tail only \n");
			}
				
		}
			
	}
	
}
