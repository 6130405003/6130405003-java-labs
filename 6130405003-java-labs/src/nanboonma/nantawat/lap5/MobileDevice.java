package nanboonma.nantawat.lap5;

import java.io.Serializable;

/**
 * This programs get information from InterestedMobileDevice and set information return toString.
 * 
 * Author: Nantawat Nanboonma
 * ID: 613040500-3
 * Sec: 2
 * Date: February 18, 2019
 *
 */

public class MobileDevice implements Serializable {
	private String modelName;
	private String os;
	private int price;
	private int weight;
	/**
	 * this method will get modelName to read and return modelName.
	 * @return
	 */
	public String getModelName() {
		return modelName;
	}
	/**
	 * this method will set modelName is information that user has given.
	 * @param modelName
	 */
	public void setModelName(String modelName) {
		this.modelName = modelName;
	}
	/**
	 * this method will get os to read and return os.
	 * @return
	 */
	public String getOs() {
		return os;
	}
	/**
	 * this method will set os is information that user has given.
	 * @param os
	 */
	public void setOs(String os) {
		this.os = os;
	}
	/**
	 * this method will get Price to read and return Price.
	 * @return
	 */
	public int getPrice() {
		return price;
	}
	/**
	 * this method will set Price is information that user has given.
	 * @param price
	 */
	public void setPrice(int price) {
		this.price = price;
	}
	/**
	 * this method will get weight to read and return weight.
	 * @return
	 */
	public int getWeight() {
		return weight;
	}
	/**
	 * this method will set weight is information that user has given.
	 * @param weight
	 */
	public void setWeight(int weight) {
		this.weight = weight;
	}
	/**
	 * this constructors will accept modelName,os,Price,weight and set information.
	 * @param modelName
	 * @param os
	 * @param price
	 * @param weight
	 */
	public MobileDevice(String modelName, String os, int price, int weight) {
		this.modelName = modelName;
		this.os = os;
		this.price = price;
		this.weight = weight;
	}
	/**
	 * this constructors will accept modelName,os,Price, and set information and weight is 0.
	 * @param modelName
	 * @param os
	 * @param price
	 */
	public MobileDevice(String modelName, String os, int price) {
		this.modelName = modelName;
		this.os = os;
		this.price = price;
		this.weight = 0;

	}
	/**
	 * this method will return each information form any Device to String.
	 */
	public String toString() {
		return ("MobileDevice [Model] name:" + modelName + ", OS: " + os + ", Price:" + price + " Baht, Weight:"
				+ weight + " g.]");

	}


}
