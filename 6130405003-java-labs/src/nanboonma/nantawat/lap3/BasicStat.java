/* BasicStat that will find the minimum, the maximum, the average and the standard deviation of the list of numbers entered. 
 * The first argument is how many numbers to be entered. The rest of the program arguments are list of numbers. 
 * Thus for program argument, the program should check if there are numbers entered as many as the first integer. 
 * The program includes two methods, namely acceptInput() and displayStats(), and two static variables, namely num[] and numInput. 
 * 		
 * 
 * Author: Nantawat Nanboonma
 * ID: 613040500-3
 * Sec: 2
 * Date: January 28, 2019
 * 
 */

package nanboonma.nantawat.lap3;
import java.util.*;
/**
 * BasicStat that will find the minimum, the maximum, the average and the standard deviation of the list of numbers entered. 
 */

public class BasicStat {
	static double num[];
	static int numInput;
	/**
	 *  Method acceptInput(), check if the number of entered numbers is the same as the first integer entered. If not, an error message 
	 * 	If the program arguments are entered correctly, set numInput to the first program argument. And the rest of numbers are to 
	 * 	be stored with double array num[].
	 */
	public static void acceptInput(String[] args) {
		
		String args_0 = args[0];
		int args_1 = Integer.parseInt(args_0);
		if (args_1 == args.length-1) {
			numInput = args_1;
			num = new double[numInput];
			
			for (int i = 0; i<= args.length-2;i++) {
				double number = Double.parseDouble(args[i+1]);
				num[i] += number;
			}
			Arrays.sort(num);
		}else {
			System.err.println("<BasicStat> <numNumbers> <numbers>");
			System.exit(0);
		}
	}
	
	/**
	 * Method displayStats(), show the minimum, the maximum, the average and the standard deviation of the numbers entered.
	 */
	 public static void displayStat() {
		
		double min = num[0];
		double max = num[numInput-1];
		double total = 0;
		for (double i : num) {
			total += i; 
		}
		double averange = total/numInput;
		double u = averange;
		double standard = 0;
		for (int i = 0;i <=numInput-1;i++) {
			standard += (Math.pow(num[i]-u,2));
		}
		double standard_deviation = Math.sqrt(standard/numInput);
		System.out.println("Max is "+max+" Min is "+min);
		System.out.println("Averange is "+ averange);
		System.out.println("Standard deviation is "+standard_deviation);			
	}
	 
	/**
	 * BasicStat that will find the minimum, the maximum, the average and the standard deviation of the list of numbers entered.
	 * @Author: Nantawat Nanboonma
	 * @Version : 11.0.1
	 */
	public static void main(String[] args) {
		acceptInput(args);
		displayStat();
	}	
}
