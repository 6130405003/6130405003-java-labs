package nanboonma.nantawat.lap5;
/**
 * this interface has create empty method for use this method to implement in another class.
 * 
 * Author: Nantawat Nanboonma
 * ID: 613040500-3
 * Sec: 2
 * Date: February 18, 2019
 *
 */
public interface DisplayTime {
	/**
	 * this is empty method for use in another class.
	 */
	void displayTime();

}
