/*
 *  A program called StringAPI that accepts one input argument (your school name) 
 *  and save that argument in the variable called schoolName  and then use the method of class String 
 *  to check whether the variable schoolName has the substring “college” (ignore cases) and “university” (ignore cases) 
 *  and then display output
 *  
* Author: Nantawat Nanboonma
* ID: 613040500-3
* Sec: 2
* Date: January 21, 2019
*
 */

package nanboonma.nantawat.lap2;

public class StringAPI {

	public static void main(String[] args) {
		String word1 = "university";
		String word2 = "college";
		if (args[0].indexOf(word1) != -1) {
			System.out.println(args[0] + " is an university");
			System.exit(0);
		} if (args[0].indexOf(word2) != -1) {
			System.out.println(args[0] + " is a college" );
		} else
			System.out.println(args[0] + " is neither an university nor a college");
	}

}
