package nanboonma.nantawat.lab9;

import javax.swing.SwingUtilities;
/**
 * This program MyFrameV8 extends from class MyFrameV7 will add paintCompenent
 * and show interface.
 * 
 * 
 * Author: Nantawat Nanboonma ID: 613040500-3 Sec: 2 Date: April 7, 2019
 * 
 * @author �ѹ��Ѳ��
 *
 */
public class MyFrameV8 extends MyFrameV7 {
	/**
	 * this is a constructor that super text from MyFrameV7 and tranform class to be
	 * like MyFrameV7.
	 */
	public MyFrameV8(String text) {
		super(text);
		// TODO Auto-generated constructor stub
	}

	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});

	}

	/**
	 * this method is the method that run JFrame process code.
	 */
	public static void createAndShowGUI() {
		MyFrameV8 msw = new MyFrameV8("My Frame V8");
		msw.addComponents();
		msw.setFrameFeatures();
	}

	/**
	 * this method will add paintComponent from MyCanvasV7 in JFrame
	 */
	protected void addComponents() {
		add(new MyCanvasV8());
	}

}
