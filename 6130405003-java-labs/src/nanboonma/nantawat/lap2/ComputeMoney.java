/*Compute how much money Wanlee has. She will input the number of notes of 1,000 Baht, 500 Baht, 100 Baht and 20 Baht, the program should output the total amount of money she has.

 * If Wanlee does not enter all four numbers for bank notes, the error message should be output as shown in Figure 7.
 * Note that to convert from a String value to a double value, you may want  to use  static method double Double.parseDouble(String s) such as  double r  = Double.parseDouble(args[0]);
 * 
* Author: Nantawat Nanboonma
* ID: 613040500-3
* Sec: 2
* Date: January 21, 2019
*
 */

package nanboonma.nantawat.lap2;

public class ComputeMoney {

	public static void main(String[] args) {
		if (args.length !=4) {
			System.err.println("ComputeMoney <1,000 Baht> <500 Baht> <100 Baht> <20 Baht>");
		} else {
			
			float insert_num1 = Float.parseFloat(args[0])*1000;
			float insert_num2 = Float.parseFloat(args[1])*500;
			float insert_num3 = Float.parseFloat(args[2])*100;
			float insert_num4 = Float.parseFloat(args[3])*20;
			float sum = insert_num1 + insert_num2 + insert_num3 +insert_num4;
			System.out.println("Total money is " + sum);
			
		}

	}

}
