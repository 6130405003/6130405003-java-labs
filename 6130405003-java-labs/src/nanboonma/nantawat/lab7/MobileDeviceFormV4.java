package nanboonma.nantawat.lab7;

import javax.swing.ImageIcon;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.SwingUtilities;

import nanboonma.nantawat.lap6.MobileDeviceFormV3;
/**
 * This program MobileDeviceFormV4 extends from class MobileDeviceFormV3 will
 * display like MobileDeviceFormV3 and just have submenu of color, size and have imageIcon of new menu.
 * 
 * Author: Nantawat Nanboonma ID: 613040500-3 Sec: 2 Date: March 5, 2019
 * 
 * @author �ѹ��Ѳ��
 *
 */
public class MobileDeviceFormV4 extends MobileDeviceFormV3 {
	/**
	 * this is image icon of new icon.
	 */
	ImageIcon newIcon;
	/**
	 * this is JMenuItem of submenu color.
	 */
	JMenuItem submenuColor;
	/**
	 * this is JMenuItem of submenu color.
	 */
	JMenuItem submenuSize; 
	public MobileDeviceFormV4(String title) {
		super(title);
	
	}
	/**
	 * this method will add sub menus in menu color and menu size.
	 */
	protected void addSubMenus() {
		menuConfig.remove(colorItem);
		menuConfig.remove(sizeItem);
		colorItem = new JMenu("Color");
		colorItem.add(new JMenuItem("Red"));
		colorItem.add(new JMenuItem("Green"));
		colorItem.add(new JMenuItem("Blue"));
		sizeItem = new JMenu("Size");
		sizeItem.add(new JMenuItem("16"));
		sizeItem.add(new JMenuItem("20"));
		sizeItem.add(new JMenuItem("24"));
		menuConfig.add(colorItem);
		menuConfig.add(sizeItem);
	}
	/**
	 * this method will update new menu to have image icon.
	 */
	protected void updateMenuIcon() {
		newItem.setIcon(new ImageIcon("images/new.jpg"));
		
	}
	/**
	 * this method will add method submenu and update menu in menu.
	 */
	protected void addMenus() {
		super.addMenus();
		updateMenuIcon();
		addSubMenus();
		
	}
	/**
	 * this method is the method that run JFrame process code.
	 */
	public static void createAndShowGUI() {
		MobileDeviceFormV4 mobileDeviceFormV4 = new MobileDeviceFormV4("Mobile Device Form V4");
		mobileDeviceFormV4.addComponents();
		mobileDeviceFormV4.addMenus();
		mobileDeviceFormV4.setFrameFeatures();

	}

	/**
	 * this main will display a result from code.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}
