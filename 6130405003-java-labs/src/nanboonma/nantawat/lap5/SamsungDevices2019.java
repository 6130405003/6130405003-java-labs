package nanboonma.nantawat.lap5;
/**
 * this programs will display an information each SamsungDevice to see.
 * 
 * Author: Nantawat Nanboonma
 * ID: 613040500-3
 * Sec: 2
 * Date: February 18, 2019
 *
 */
public class SamsungDevices2019 {
	/**
	 * this method will proceed an information and run code to see information.
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		MobileDevice s9 = new SamsungDevice("Galaxy S9", 23900, 163, 8.0);
		SamsungDevice note9 = new SamsungDevice("Galaxy Note9", 25500, 201, 8.1);
		System.out.println("I would like to have");
		System.out.println(note9);
		System.out.println("But to save money, I should buy ");
		System.out.println(s9);
		double diff = note9.getPrice() - s9.getPrice();
		System.out.println("Samsung Galaxy Note 9 is more expensive than Samsung Galaxy S9 by " + diff + " Baht.");
		System.out.println("Both these device have the same brand which is " + SamsungDevice.getBrand());

	}

}
