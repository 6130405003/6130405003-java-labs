/*
 * Interface Moveable has three methods. The methods are accelerate(), brake() and setSpeed().
 * 
 * Author: Nantawat Nanboonma
 * ID: 613040500-3
 * Sec: 2
 * Date: February 8, 2019
 * 
 */
package nanboonma.nantawat.lap4;
/**
 * Interface Moveable has three methods. The methods are accelerate(), brake() and setSpeed().
 * @author �ѹ��Ѳ��
 *
 */
public interface Movable {
	public void accelarate();
	public void brake();
	public void setSpeed(int speed);
	
}
