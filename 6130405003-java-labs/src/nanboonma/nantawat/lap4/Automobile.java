/*
 * this program will process information of each car and return details to String.
 * 
 * Author: Nantawat Nanboonma
 * ID: 613040500-3
 * Sec: 2
 * Date: February 8, 2019 
 */
package nanboonma.nantawat.lap4;

import java.lang.*;
/**
 * this class have get information and set to object then return to String.
 * @author �ѹ��Ѳ��
 *
 */
public class Automobile {
	private int gasoline;
	private int speed;
	private int maxSpeed;
	private int acceleration;
	private static int numberOfAutomobile = 0;
	private String model;
	private Color color;
	
	/**
	 * this enum has collected the color of car.
	 * @author �ѹ��Ѳ��
	 *
	 */
	public enum Color {RED, ORANGE, YELLOW, GREEN, BLUE, INDIGO, VIOLET, WHITE, BLACK}

	public static void setNumberOfAutomobile(int numberOfAutomobile) {
		Automobile.numberOfAutomobile = numberOfAutomobile;
	}

	public int getGasoline() {
		return gasoline;
	}

	public int getSpeed() {
		return speed;
	}

	public int getMaxSpeed() {
		return maxSpeed;
	}

	public int getAcceleration() {
		return acceleration;
	}

	public static int getNumberOfAutomobile() {
		return numberOfAutomobile;
	}

	public String getModel() {
		return model;
	}

	public Color getColor() {
		return color;
	}

	public void setGasoline(int gasoline) {
		this.gasoline = gasoline;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

	public void setAcceleration(int accelleration) {
		this.acceleration = accelleration;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public void setMaxSpeed(int maxSpeed) {
		this.maxSpeed = maxSpeed;
	}

	public Automobile() {
		this.gasoline = 0;
		this.speed = 0;
		this.maxSpeed = 160;
		this.acceleration = 0;
		this.model = "Automobile";
		this.color = Color.WHITE;
		numberOfAutomobile++;
	}
	public Automobile(int gasoline, int speed, int maxSpeed, int acceleration, String model, Color color) {
		this.gasoline = gasoline;
		this.speed = speed;
		this.maxSpeed = maxSpeed;
		this.acceleration = acceleration;
		this.model = model;
		this.color = color;
		numberOfAutomobile++;
	}

	

	public String toString() {
		return ("Automobile [gasoline=" + gasoline + ", speed=" + speed + ", maxSpeed=" + maxSpeed + ", acceleration="
				+ acceleration + ", model=" + model + ", color=" + color + "]");
	}
}
