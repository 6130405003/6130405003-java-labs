package nanboonma.nantawat.lap5;
/**
 * This programs get information from AndroidDevice2019 and set information return toString.
 * 
 * Author: Nantawat Nanboonma
 * ID: 613040500-3
 * Sec: 2
 * Date: February 18, 2019
 *
 */
public class AndroidSmartWatch extends AndroidDevice implements DisplayTime {
	private String modelName;
	private String brandName;
	private int price;
	/**
	 * this constructors will accept modelName,brandName,Price and set information.
	 * @param modelName
	 * @param brandName
	 * @param price
	 */
	public AndroidSmartWatch(String modelName, String brandName, int price) {
		this.modelName = modelName;
		this.brandName = brandName;
		this.price = price;
	}
	/**
	 * this method will get modelName to read and return modelName.
	 * @return
	 */
	public String getModelName() {
		return modelName;
	}
	/**
	 * this method will set modelName is information that user has given.
	 * @param modelName
	 */
	public void setModelName(String modelName) {
		this.modelName = modelName;
	}
	/**
	 * this method will get BrandName to read and return BrandName.
	 * @return
	 */
	public String getBrandName() {
		return brandName;
	}
	/**
	 * this method will set brandName is information that user has given.
	 * @param brandName
	 */
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
	/**
	 * this method will get Price to read and return Price.
	 * @return
	 */
	public int getPrice() {
		return price;
	}
	/**
	 * this method will set Price is information that user has given.
	 * @param price
	 */
	public void setPrice(int price) {
		this.price = price;
	}
	/**
	 * this method will return each information form any Device to String.
	 */
	public String toString() {
		
		return ("AndroidSmartWatch[Brand name:" + getBrandName() + ", Model name: " + getModelName() + ", Price:"
				+ getPrice() + " Baht.");

	}

	String usage() {
		System.out.println("AndroidSmartWatch Usage: Show time, date, your heart rate, and your step count");
		return null;

	}
	/**
	 * this method will display string to see.
	 */
	public void displayTime() {
		System.out.println("Display time only using a digital format");
		
	}
}
