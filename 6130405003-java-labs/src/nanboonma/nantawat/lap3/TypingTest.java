/*  TypingTest is a program that test whether a user types faster or slower than an average person. 
 An average person types at the speed of 40 words per minute. The program randoms 8 colors from a list of rainbow colors 
(RED, ORANGE, YELLOW, GREEN, BLUE, INDIGO, VIOLET). Color can be picked more than once. A user must type in the same list ignoring 
case. If the user type in incorrectly, he needs to type in again. After the correct input is enter, the time used for typing in 
will be calculated and if he types in faster than or equal to 12 seconds, he types faster than average otherwise he types slower 
than average. The time for typing in can be computed using subroutine currentTimeMillis in class System. Note that the time for 
incorrect typing in will be added to the total typing time.
 *
 * Author: Nantawat Nanboonma
 * ID: 613040500-3
 * Sec: 2
 * Date: January 28, 2019
 * 
 */

package nanboonma.nantawat.lap3;
import java.util.*;
/**
 * TypingTest is a program that test whether a user types faster or slower than an average person with random 8 colors.
 */
public class TypingTest {
	/**
	 * TypingTest is a program that test whether a user types faster or slower than an average person with random 8 colors.
	 * @Author: Nantawat Nanboonma
	 * @Version : 11.0.1
	 */
		
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		String [] list_color = {"RED","ORANGE","YELLOW","GREEN","BLUE","INDIGO","VIOLET"};
		Random random = new Random();
		
		String random_color = "";
		for (int i = 0; i<=list_color.length;i++) {
			int random_num = random.nextInt(list_color.length);
			random_color += list_color[random_num]+" ";	
		}
		
		String random_color_trim = random_color.trim();
		System.out.println(random_color);
		
		while (true) {
			Scanner input_text = new Scanner(System.in);
			System.out.print("Type your answer: ");
			String text_input = input_text.nextLine();
			String upper_text = text_input.toUpperCase();
				
			long finish = System.currentTimeMillis();
			double elapsed_time = (finish - start);
					
			if (upper_text.equals(random_color_trim)) {
				System.out.printf("Your time is "+ elapsed_time/1000+" second. \n");
				if (elapsed_time/1000 <= 12) {
					System.out.println("You type faster than everage person.");
					System.exit(0);
				}else {
					System.out.println("You type slower than everage person.");
					System.exit(0);
				}
			}else {
				continue;
			}
			}
		}
}
	
		
	