package nanboonma.nantawat.lab8;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

/**
 * This program MyCanvasV2 extends from class MyCanvas will draw and fill
 * ball,pedal,brick and line.
 *
 * 
 * Author: Nantawat Nanboonma ID: 613040500-3 Sec: 2 Date: March 28, 2019
 * 
 * @author �ѹ��Ѳ��
 *
 */
public class MyCanvasV2 extends MyCanvas {
	/**
	 * this is constructors of circle to draw or fill ball.
	 */
	protected MyBall ball;
	/**
	 * this is constructors of rectangle to draw or fill pedal.
	 */
	protected MyPedal pedal;
	/**
	 * this is constructors of rectangle to draw or fill brick.
	 */
	protected MyBrick brick;

	/**
	 * this method will draw shape Graphics2D
	 */
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(Color.BLACK);
		g.fillRect((getWidth() - 300) / 2, (getHeight() - 300) / 2, WIDTH, HEIGHT);
		ball = new MyBall(400 - (MyBall.diameter / 2), 300 - (MyBall.diameter / 2));
		brick = new MyBrick(400 - (MyBrick.brickWidth / 2), 0);
		pedal = new MyPedal(400 - (MyPedal.pedalWigth / 2), HEIGHT - MyPedal.pedalHeight);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setColor(Color.WHITE);
		g2d.fill(ball);
		g2d.fill(brick);
		g2d.fill(pedal);
		g2d.drawLine(0, 300, 800, 300);
		g2d.drawLine(400, 0, 400, 600);
	}

}
