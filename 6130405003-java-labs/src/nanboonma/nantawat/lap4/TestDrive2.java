/* The program TestDrive2 get max speed,accelerate and model of automobile and display gasoline, speed,max speed,acceleration of 
 * each automobile that get in program.
 * 
 * Author: Nantawat Nanboonma
 * ID: 613040500-3
 * Sec: 2
 * Date: February 8, 2019
 */
package nanboonma.nantawat.lap4;

/**
 * The program TestDrive get max speed,accelerate and model of automobile and display gas, speed,max speed,acceleration of 
 * each automobile that get in program.
 * 
 * @author �ѹ��Ѳ��
 *
 */
public class TestDrive2 {

	/**
	 * this method has check information each class and check what a car is faster
	 * than.
	 * 
	 * @param car1
	 * @param car2
	 */
	static void isFaster(ToyotaAuto car1, HondaAuto car2) {
		int speed_honda = car2.getSpeed();
		int speed_toyota = car1.getSpeed();
		String model_honda = car2.getModel();
		String model_toyota = car1.getModel();
		if (speed_toyota > speed_honda) {
			System.out.println(model_toyota + " is faster than " + model_honda);
		} else if (speed_toyota < speed_honda) {
			System.out.println(model_toyota + " is NOT faster than " + model_honda);
		} else {
			System.out.println(model_toyota + "has the same speed as " + model_honda);
		}
	}

	/**
	 * this method has check information each class and check what a car is faster than.
	 * 
	 * @param car1
	 * @param car2
	 */
	static void isFaster(HondaAuto car2, ToyotaAuto car1) {
		int speed_honda = car2.getSpeed();
		int speed_toyota = car1.getSpeed();
		String model_honda = car2.getModel();
		String model_toyota = car1.getModel();
		if (speed_honda > speed_toyota) {
			System.out.println(model_honda + " is faster than " + model_toyota);
		} else if (speed_honda < speed_toyota) {
			System.out.println(model_honda + "is NOT faster than " + model_toyota);
		} else {
			System.out.println(model_honda + "has the same speed as " + model_toyota);
		}
	}

	/**
	 * This main will be display operation of accelerate brakes and refuel.
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ToyotaAuto car1 = new ToyotaAuto(200, 10, "Vios");
		HondaAuto car2 = new HondaAuto(220, 8, "City");

		System.out.println(car1);
		System.out.println(car2);

		car1.accelarate();
		car2.accelarate();
		car2.accelarate();
		System.out.println(car1);
		System.out.println(car2);

		car1.brake();
		car1.brake();
		car2.brake();
		System.out.println(car1);
		System.out.println(car2);

		car1.refuel();
		car2.refuel();
		System.out.println(car1);
		System.out.println(car2);

		isFaster(car1, car2);
		isFaster(car2, car1);
	}

}
