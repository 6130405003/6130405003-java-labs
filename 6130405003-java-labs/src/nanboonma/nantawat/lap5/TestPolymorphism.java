package nanboonma.nantawat.lap5;
/**
 * this programs will take source for any class and run in here.
 * 
 * Author: Nantawat Nanboonma
 * ID: 613040500-3
 * Sec: 2
 * Date: February 18, 2019
 * 
 */
public class TestPolymorphism {
	/**
	 * this method will proceed code, run code and display result.
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		AndroidDevice ticwatchPro = new AndroidSmartWatch("Mobvoi", "TicWatch Pro", 8390);
		SamsungDevice note9 = new SamsungDevice("Galaxy Note9", 25500, 201, 8.1);
		System.out.println(ticwatchPro);
		((AndroidSmartWatch) ticwatchPro).displayTime();
		System.out.println();
		System.out.println(note9);
		note9.displayTime();
		
	}

}
