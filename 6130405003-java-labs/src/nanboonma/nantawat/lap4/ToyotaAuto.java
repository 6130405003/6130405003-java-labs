/* The program ToyotaAuto will extends Automobile and implement Movable and Refuelable and process information of Toyota car.
 * 
 * Author: Nantawat Nanboonma
 * ID: 613040500-3
 * Sec: 2
 * Date: February 8, 2019
 */
package nanboonma.nantawat.lap4;

/**
 * The program ToyotaAuto will extends Automobile and implement Movable and Refuelable and process information of Toyota car.
 * @author �ѹ��Ѳ��
 *
 */
public class ToyotaAuto extends Automobile implements Movable, Refuelable {
	/**
	 * the method refuel() will sets the value of gasoline to 100 and displays a
	 * message �refuels� .
	 */
	public void refuel() {
		String model_refuel = this.getModel();
		this.setGasoline(100);
		System.out.println(model_refuel + " refuels");
	}

	/**
	 * the method accelerate() that increases the current speed by the accelerationt
	 * and the method decreases gasoline by 15. The method also displays message
	 * �accelerates�.
	 */
	public void accelarate() {
		String model_accel = this.getModel();
		int gasoline_accel = this.getGasoline();
		int speed_accel = this.getSpeed();
		int acceleration_accel = this.getAcceleration();
		int maxspeed_accel = this.getMaxSpeed();
		if (speed_accel + acceleration_accel > maxspeed_accel) {
			this.setSpeed(maxspeed_accel);
		} else {
			this.setSpeed(speed_accel + acceleration_accel);
		}
		this.setGasoline(gasoline_accel - 15);
		System.out.println(model_accel + " accelerate");
	}

	/**
	 * the method brake() will decreases the current speed by the acceleration. The
	 * method will check if the new speed is not less than 0. Otherwise set the new
	 * speed to 0. The method decreases the value of gasoline by 15. The method also
	 * displays message �brakes�
	 */
	public void brake() {
		String model_brake = this.getModel();
		int gasoline_brake = this.getGasoline();
		int speed_brake = this.getSpeed();
		int acceleration_brake = this.getAcceleration();
		if (speed_brake - acceleration_brake < 0) {
			this.setSpeed(0);
		} else {
			this.setSpeed(speed_brake - acceleration_brake);
		}
		this.setGasoline(gasoline_brake - 15);
		System.out.println(model_brake + " brakes");
	}

	/**
	 * this object has get max speed acceleration and model of car and set
	 * information of them.
	 * 
	 * @param maxspeed
	 * @param acceleration
	 * @param model
	 */
	ToyotaAuto(int maxspeed, int acceleration, String model) {
		this.setGasoline(100);
		this.setMaxSpeed(maxspeed);
		this.setAcceleration(acceleration);
		this.setModel(model);

	}

	/**
	 * the method setSpeed(int) that accepts one integer for setting the speed. The
	 * method will check if the input speed must neither be negative nor exceed
	 * maxSpeed. Otherwise set speed to 0 or maxSpeed accordingly.
	 */
	public void setSpeed(int speed) {
		int max_speed = this.getMaxSpeed();
		if (speed < 0) {
			super.setSpeed(0);

		} else if (speed > max_speed) {
			super.setSpeed(max_speed);
		} else {
			super.setSpeed(speed);
		}
	}

	/**
	 * This method will be return information to String and display.
	 */
	public String toString() {

		String model_toyota = this.getModel();
		int gasoline_tostring = this.getGasoline();
		int speed_tostring = this.getSpeed();
		int acceleration_tostring = this.getAcceleration();
		int maxspeed_tostring = this.getMaxSpeed();

		return (model_toyota + " gas:" + gasoline_tostring + " speed:" + speed_tostring + " max speed:"
				+ maxspeed_tostring + " acceleration:" + acceleration_tostring);
	}
}
