package nanboonma.nantawat.lab10;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.JMenuItem;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;

/**
 * This program MobileDeviceFormV8 extends from class MobileDeviceFormV7 program
 * will set accelerators and mnemonic keys to menu and sub menu
 *
 * 
 * Author: Nantawat Nanboonma ID: 613040500-3 Sec: 2 Date: April 16, 2019
 * 
 * @author �ѹ��Ѳ��
 *
 */

public class MobileDeviceFormV8 extends MobileDeviceFormV7 {
	/**
	 * this is sub menu of custom.
	 */
	protected JMenuItem custom;
	/**
	 * this is sub menu of red.
	 */
	protected JMenuItem redItem;
	/**
	 * this is sub menu of blue.
	 */
	protected JMenuItem blueItem;
	/**
	 * this is sub menu of green.
	 */
	protected JMenuItem greenItem;

	/**
	 * this is constructors of MobileDeviceFormV8 that super code from
	 * MobileDeviceFormV7.
	 */
	public MobileDeviceFormV8(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}

	/**
	 * this method will add sub menu in menu bar.
	 */
	protected void addSubMenus() {
		super.addSubMenus();
		colorItem.removeAll();
		redItem = new JMenuItem("Red");
		greenItem = new JMenuItem("Green");
		blueItem = new JMenuItem("Blue");
		custom = new JMenuItem("Custom...");
		colorItem.add(redItem);
		colorItem.add(greenItem);
		colorItem.add(blueItem);
		colorItem.add(custom);

	}

	/**
	 * this method will set set accelerators keys with aKey and CTRL_MASK and
	 * mnemonic keys with mKey.
	 * 
	 * @param menu
	 * @param mKey
	 * @param aKey
	 */
	public void setMAKeys(JMenuItem menu, int mKey, int aKey) {
		menu.setMnemonic(mKey);
		menu.setAccelerator(KeyStroke.getKeyStroke(aKey, ActionEvent.CTRL_MASK));

	}

	/**
	 * this method will increase aKey and mKey form method setMAKeys with KeyEvent.
	 */
	public void enableKeyboard() {
		menuFile.setMnemonic(KeyEvent.VK_F);
		menuConfig.setMnemonic(KeyEvent.VK_C);
		colorItem.setMnemonic(KeyEvent.VK_L);
		setMAKeys(newItem, KeyEvent.VK_N, KeyEvent.VK_N);
		setMAKeys(openItem, KeyEvent.VK_O, KeyEvent.VK_O);
		setMAKeys(saveItem, KeyEvent.VK_S, KeyEvent.VK_S);
		setMAKeys(exitItem, KeyEvent.VK_X, KeyEvent.VK_X);
		setMAKeys(redItem, KeyEvent.VK_R, KeyEvent.VK_R);
		setMAKeys(greenItem, KeyEvent.VK_G, KeyEvent.VK_G);
		setMAKeys(blueItem, KeyEvent.VK_B, KeyEvent.VK_B);
		setMAKeys(custom, KeyEvent.VK_U, KeyEvent.VK_U);

	}

	/**
	 * this method is the method that run JFrame process code.
	 */
	public static void createAndShowGUI() {
		MobileDeviceFormV8 mobileDeviceFormV8 = new MobileDeviceFormV8("Mobile Device Form V8");
		mobileDeviceFormV8.addComponents();
		mobileDeviceFormV8.initComponents();
		mobileDeviceFormV8.addMenus();
		mobileDeviceFormV8.enableKeyboard();
		mobileDeviceFormV8.addListeners();
		mobileDeviceFormV8.setFrameFeatures();

	}

	/**
	 * this main will display a result from code.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}
