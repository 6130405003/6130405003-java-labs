package nanboonma.nantawat.lap5;

/**
 * This programs abstract class is an empty method There is only the head of the
 * method. We use abstract class to create it as a parent class.
 * 
 * Author: Nantawat Nanboonma
 * ID: 613040500-3
 * Sec: 2
 * Date: February 18, 2019
 * 
 */
public abstract class AndroidDevice {
	/**
	 * this is abstract method create for Android take it to use.
	 * 
	 */
	abstract String usage();
	/**
	 * this private static constant class member is set os be "Android"
	 */
	private static final String os = "Android";
	/**
	 * this method will get os to read and return os.
	 * @return
	 */
	public static String getOs() {
		return os;
	}

}
