package nanboonma.nantawat.lap6;

import java.awt.*;
import javax.swing.*;
import javax.swing.*;

/**
 * This program MobileDeviceFormV1 extends from class MySimpleWindow will
 * display like MySimpleWindow and have textFeild of brand , model ,weight
 * ,price and have os radiobutton .
 * 
 * Author: Nantawat Nanboonma ID: 613040500-3 Sec: 2 Date: February 27, 2019
 * 
 */

public class MobileDeviceFormV1 extends MySimpleWindow {
	/**
	 * this is textField of brand name.
	 */
	protected JTextField firstTxtField;
	/**
	 * this is textField of model name.
	 */
	protected JTextField secondTxtField;
	/**
	 * this is textField of weight.
	 */
	protected JTextField thirdTxtField;
	/**
	 * this is textField of price.
	 */
	protected JTextField fourthTxtField;

	/**
	 * this is lapel of brand name.
	 */
	protected JLabel firstnameLabel;
	/**
	 * this is lapel of model name.
	 */
	protected JLabel secondnameLabel;
	/**
	 * this is lapel of weight.
	 */
	protected JLabel thirdnameLabel;
	/**
	 * this is lapel of price.
	 */
	protected JLabel fourthnameLabel;
	/**
	 * this is lapel of mobile OS.
	 */
	protected JLabel mobileOsLabel;

	/**
	 * this is panel was made for get all label and all textField.
	 */
	protected JPanel panelGrid;
	/**
	 * this is panel was made for take radio button to get in.
	 */
	protected JPanel radioPanel;
	/**
	 * this is radio button of android OS.
	 */
	protected JRadioButton androidOs;
	/**
	 * this is radio button of iOS.
	 */
	protected JRadioButton ios;

	/**
	 * this is a constructor that super title from MySimpleWindow and tranfrom class
	 * to be like MySimpleWindow.
	 * 
	 * @param title
	 */
	public MobileDeviceFormV1(String title) {
		super(title);

	}

	/**
	 * this method is the operation like MySimpleWindow and get label ,textFeild and
	 * radio button.
	 */
	protected void addComponents() {
		super.addComponents();
		panelGrid = new JPanel();
		radioPanel = new JPanel();
		panelGrid.setLayout(new GridLayout(0, 2, 5, 5));
		firstTxtField = new JTextField(15);
		secondTxtField = new JTextField(15);
		thirdTxtField = new JTextField(15);
		fourthTxtField = new JTextField(15);
		firstnameLabel = new JLabel("Brand Name: ");
		secondnameLabel = new JLabel("Model Name: ");
		thirdnameLabel = new JLabel("Weight (kg.): ");
		fourthnameLabel = new JLabel("Price (Baht): ");
		mobileOsLabel = new JLabel("Mobile OS:");
		androidOs = new JRadioButton("Android");
		ios = new JRadioButton("iOS");
		ButtonGroup osGroup = new ButtonGroup();
		osGroup.add(androidOs);
		osGroup.add(ios);
		radioPanel.add(androidOs);
		radioPanel.add(ios);

		panelGrid.add(firstnameLabel);
		panelGrid.add(firstTxtField);
		panelGrid.add(secondnameLabel);
		panelGrid.add(secondTxtField);
		panelGrid.add(thirdnameLabel);
		panelGrid.add(thirdTxtField);
		panelGrid.add(fourthnameLabel);
		panelGrid.add(fourthTxtField);
		panelGrid.add(mobileOsLabel);
		panelGrid.add(radioPanel);
		panel.add(panelGrid, BorderLayout.NORTH);

	}

	/**
	 * this method is the method that run JFrame process code.
	 */
	public static void createAndShowGUI() {
		MobileDeviceFormV1 mobileDeviceFormV1 = new MobileDeviceFormV1("Mobile Device Form V1");
		mobileDeviceFormV1.addComponents();
		mobileDeviceFormV1.setFrameFeatures();

	}

	/**
	 * this main will display a result from code.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}
