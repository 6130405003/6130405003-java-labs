package nanboonma.nantawat.lab9;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

import nanboonma.nantawat.lab8.MyBall;
/**
 * This program MyCanvasV6 extends from class MyCanvasV5 will draw ball
 * and ball can run in panel
 *
 * 
 * Author: Nantawat Nanboonma ID: 613040500-3 Sec: 2 Date: April 7, 2019
 * 
 * @author �ѹ��Ѳ��
 *
 */
public class MyCanvasV6 extends MyCanvasV5 implements Runnable {
	/**
	 * this is constructors of circle to draw or fill ball.
	 */
	protected MyBallV2 ball2 = new MyBallV2(super.WIDTH / 2 - MyBall.diameter / 2,
			super.HEIGHT / 2 - MyBall.diameter / 2);
	/**
	 * this is constructor of thread to order ball to run.
	 */
	protected Thread running2 = new Thread(this);
	/**
	 * this is constructors of MyCavasV6 to set Velocity of ball and run ball.
	 */
	public MyCanvasV6() {
		ball2.ballVelX = 1;
		ball2.ballVelY = 1;
		running2.start();
	}
	/**
	 * this method will draw shape Graphics2D.
	 */
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setColor(Color.BLACK);
		g2d.fillRect(0, 0, super.WIDTH, super.HEIGHT);
		g2d.setColor(Color.WHITE);
		g2d.fill(ball2);
	}
	/**
	 * this method is made for write code for ball to run.
	 */
	public void run() {
		while (true) {
			if (ball2.x + MyBall.diameter >= super.WIDTH) {
				ball2.x = super.WIDTH - MyBall.diameter;
				ball2.ballVelX *= -1;

			} else if (ball2.x + MyBall.diameter <= 30) {
				ball2.x = 0;
				ball2.ballVelX *= -1;

			} else if (ball2.y + MyBall.diameter >= super.HEIGHT) {
				ball2.y = super.HEIGHT - MyBall.diameter;
				ball2.ballVelY *= -1;

			} else if (ball2.y + MyBall.diameter <= 30) {
				ball2.y = 0;
				ball2.ballVelY *= -1;

			}

			ball2.move();
			repaint();

			try {
				Thread.sleep(1);
			} catch (InterruptedException ex) {
			}
			// TODO Auto-generated method stub

		}

	}

}
